#!/usr/bin/env python3
from __future__ import print_function
import sys

def WARN(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

def LOG(*objs):
    print(*objs)

