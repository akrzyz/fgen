#!/usr/bin/env python3

# This Classes isolate rest of Code from types produced by CppHeaderParser,
# which is external lib and hopefully will be changed for something better

import re
from .consts import *

class CppHeader :
    def __init__ (self, headerRawData) :
        self.__classes = []
        for classData in headerRawData.classes.values() :
            self.__classes.append(CppClass(classData))

        self.__freeFunctions = []
        for freeFunctionData in headerRawData.functions :
            self.__freeFunctions.append(CppFunction(freeFunctionData))

    def classes(self) :
        return self.__classes

    def freeFunctions(self) :
        return self.__freeFunctions
    #utils
    def __repr__(self) :
        return repr(vars(self))

    def __eq__(self, other) :
        if isinstance(other, type(self)) :
            return vars(self) == vars(other)
        else :
            raise TypeError

class CppClass :
    def __init__ (self, classRawData) :
        self.__name = classRawData[NAME]
        self.__namespace = classRawData[NAMESPACE]

        self.__publicMethods = []
        for rawMethodData in classRawData[METHODS][ACCESS_PUBLIC] :
            self.__publicMethods.append(CppFunction(rawMethodData))

        self.__protectedMethods = []
        for rawMethodData in classRawData[METHODS][ACCESS_PROTECTED] :
            self.__protectedMethods.append(CppFunction(rawMethodData))

        self.__privateMethods = []
        for rawMethodData in classRawData[METHODS][ACCESS_PRIVARE] :
            self.__privateMethods.append(CppFunction(rawMethodData))

    def name(self) :
        return self.__name

    def namespace(self) :
        return self.__namespace

    def publicMethods(self) :
        return self.__publicMethods

    def protectedMethods(self) :
        return self.__protectedMethods

    def privateMethods(self) :
        return self.__privateMethods

    def methods(self) :
        return (self.__publicMethods,
                self.__protectedMethods,
                self.__privateMethods)
    #utils
    def __repr__(self) :
        return repr(vars(self))

    def __eq__(self, other) :
        if isinstance(other, type(self)) :
            return vars(self) == vars(other)
        else :
            raise TypeError

class CppFunction :
    def __init__(self, rawMethodData) :
        self.__name = rawMethodData[NAME]
        self.__rtnType = rawMethodData[RETURN_TYPE]
        self.__isVirtual = rawMethodData[VIRTUAL]
        self.__isConst = rawMethodData[CONST]
        self.__isStatic = rawMethodData[STATIC]
        self.__isPureVirtual = rawMethodData[PURE_VIRTUAL]
        self.__isConstructor = rawMethodData[CTOR]
        self.__isDestructor = rawMethodData[DTOR]
        self.__isOperator = True if re.match(r"^operator", rawMethodData[NAME]) else False
        self.__isDeclaration = rawMethodData[DEBUG][-1] == ";"

        self.__parameters = []
        for rawParamData in rawMethodData[PARAMETERS] :
            self.__parameters.append(CppParameter(rawParamData))

    #base info
    def name(self) :
        return self.__name

    def returnType(self) :
        return self.__rtnType

    def parameters(self) :
        return self.__parameters
    #checkers
    def isVirtual(self) :
        return self.__isVirtual

    def isPureVirtual(self) :
        return self.__isPureVirtual

    def isConst(self) :
        return self.__isConst

    def isStatic(self) :
        return self.__isStatic

    def isConstructor(self) :
        return self.__isConstructor

    def isDestructor(self) :
        return self.__isDestructor

    def isOperator(self) :
        return self.__isOperator

    def isRegularMethod(self) :
        return not self.isConstructor() and \
               not self.isDestructor() and  \
               not self.isOperator()

    def isDeclaration(self):
        return self.__isDeclaration

    def isDefinition(self):
        return not self.isDeclaration()
    #utils
    def __repr__(self) :
        return repr(vars(self))

    def __eq__(self, other) :
        if isinstance(other, type(self)) :
            return vars(self) == vars(other)
        else :
            raise TypeError

class CppParameter :
    def __init__(self, rawParamData) :
        self.__type = rawParamData[TYPE]
        self.__name = rawParamData[NAME]

    def type(self) :
        return self.__type

    def name(self) :
        return self.__name
    #utils
    def __repr__(self) :
        return repr(vars(self))

    def __eq__(self, other) :
        if isinstance(other, type(self)) :
            return vars(self) == vars(other)
        else :
            raise TypeError

