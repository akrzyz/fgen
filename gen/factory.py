#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *

class FactoryInterfaceFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes
        self.__fileName = p_fileName

    def create(self) :
        classes = []
        for _class in self.__classes :
            classes.append(FactoryInterfaceFile(_class, self.__fileName))
        return classes

class FactoryInterfaceFile:
    def __init__(self, p_class, p_file) :
        self.__class = p_class
        self.__file = p_file
        self.__name = addILikieInterface(p_class.name() + FACTORY_NAME_POSTFIX)
        self.name = createFileName(p_class.namespace(), self.__name, HPP)

    def __str__(self) :
        local_includes = [self.__file]
        global_includes = [STD_MEMORY]
        namespace = self.__class.namespace()
        content = str(factoryInterface(self.__class.name()))
        return createHeaderFile(content, namespace, local_includes, global_includes)

def factoryInterface(p_productedClassName) :
    factoryName = addILikieInterface(p_productedClassName + FACTORY_NAME_POSTFIX)
    factoryContent =  ACCESS_PUBLIC + ":\n"
    factoryContent += INDENT + virtualDestructor(factoryName)
    factoryContent += INDENT + factoryMethodCreate(p_productedClassName)
    return str(Class(factoryName, factoryContent))

def factoryMethodCreate(p_productedClassName) :
    return "virtual std::shared_ptr<%s> create() = 0;\n" % p_productedClassName

