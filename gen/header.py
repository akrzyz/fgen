#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *
from .CppPrinters import *

class HeaderFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes
        self.__fileName = p_fileName

    def create(self) :
        classes = []
        for _class in self.__classes :
            if hasAbstractMethodsOrVirtualDestructor(_class) :
                classes.append(HeaderFile(_class, self.__fileName))
        return classes

class HeaderFile:
    def __init__(self, p_class, p_file) :
        self.__class = p_class
        self.__file = p_file
        self.__className = removeILikieInterface(p_class.name())
        self.name = createFileName(p_class.namespace(), self.__className, HPP)

    def __str__(self) :
        content   = str(HeaderClass(self.__class, self.__className))
        namespace = self.__class.namespace()
        includes  = [self.__file]
        return createHeaderFile(content, namespace, includes)

class HeaderClass:
    def __init__(self, p_interface, p_implClassName) :
        self.interface = p_interface
        self.implClassName = p_implClassName

    def getMethods(self):
        publicMethods    = self.createMethodsStrAndAddAccesSpecifier(self.interface.publicMethods(), ACCESS_PUBLIC)
        protectedMethods = self.createMethodsStrAndAddAccesSpecifier(self.interface.protectedMethods(), ACCESS_PROTECTED)
        privateMethods   = self.createMethodsStrAndAddAccesSpecifier(self.interface.privateMethods(), ACCESS_PRIVARE)
        return publicMethods + protectedMethods + privateMethods

    def createMethodsStrAndAddAccesSpecifier(self, p_methods, p_accesType) :
        methods = self.createMethodsStr(p_methods)
        if methods : methods = p_accesType + ":\n" + methods
        return methods

    def createMethodsStr(self, p_methods) :
        methods = ""
        for method in p_methods :
            if method.isPureVirtual() :
                methods += INDENT + FunctionPrinter(method).override()
        return methods;

    def __str__(self) :
        name = self.implClassName
        content = self.getMethods()
        inheritances = {ACCESS_PUBLIC : [self.interface.name()]}
        return str(Class(name, content, inheritances))

def hasAbstractMethodsOrVirtualDestructor(p_class) :
   for methods in p_class.methods() :
       for method in methods :
           if method.isPureVirtual() or (method.isVirtual() and method.isDestructor()) :
               return True
   return False

