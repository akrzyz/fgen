#!/usr/bin/env python3

INDENT = "    "

UT_NESTED_NAMESPACE = "" #now longer namespace "Ut"

METHODS = "methods"
NAME = "name"
TYPE = "type"
RETURN_TYPE = "rtnType"
PURE_VIRTUAL = "pure_virtual"
PARAMETERS = "parameters"
FUNCTION_NAME = "funcName"
CTOR = "constructor"
DTOR = "destructor"
DEBUG = "debug"

NAMESPACE = "namespace"
CLASS = "class"
VIRTUAL = "virtual"
CONST = "const"
OVERRIDE = "override"
STATIC = "static"

ACCESS_PUBLIC = "public"
ACCESS_PRIVARE = "private"
ACCESS_PROTECTED = "protected"

TEST_SUITE_NAME_POSTFIX = "TestSuite"
MOCK_NAME_POSTFIX = "Mock"

GTEST_HEADER = "gtest/gtest.h"
GMOCK_HEADER = "gmock/gmock.h"

GTEST_NAMESPACE = "::testing"
GTEST_SUITE_BASE_CLASS = "Test"

FACTORY_NAME_POSTFIX = "Factory"
STD_MEMORY = "memory" #include for std::shared_ptr

CPP = "cpp"
HPP = "hpp"

#output directories
DEFAULT_OUTPUT_DIR_INTERFACE = "./Include"
DEFAULT_OUTPUT_DIR_HEADER = "./Include"
DEFAULT_OUTPUT_DIR_IMPLEMENTATION = "./Source"
DEFAULT_OUTPUT_DIR_FACTORY_INTERFACE = "./Include"
DEFAULT_OUTPUT_DIR_TEST_SUITE = "./Test_modules"
DEFAULT_OUTPUT_DIR_MOCK = "./Test_modules/Mocks"

