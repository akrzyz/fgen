#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *
from .CppPrinters import *

class ExtractedInterfacesFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes

    def create(self) :
        classes = []
        for _class in self.__classes :
            classes.append(ExtractedInterfaceFile(_class))
        return classes

class ExtractedInterfaceFile:
    def __init__(self, p_class) :
        self.__class = p_class
        self.__className = addILikieInterface(p_class.name())
        self.name = createFileName(p_class.namespace(), self.__className, HPP)

    def __str__(self) :
        content   = str(ExtractedInterfaceClass(self.__class, self.__className))
        namespace = self.__class.namespace()
        return createHeaderFile(content, namespace)

class ExtractedInterfaceClass:
    def __init__(self, p_classWithInterfaceToExtract, p_name) :
        self.classWithInterfaceToExtract = p_classWithInterfaceToExtract
        self.name = addILikieInterface(p_classWithInterfaceToExtract.name())

    def createMethodsStr(self) :
        methods =  ACCESS_PUBLIC + ":\n"
        methods += INDENT + virtualDestructor(self.name)
        for method in self.classWithInterfaceToExtract.publicMethods() :
            if not method.isStatic() and not method.isConstructor() and not method.isDestructor():
                methods += INDENT + FunctionPrinter(method).interface()
        return methods;

    def __str__(self) :
        name = self.name
        content = self.createMethodsStr()
        return str(Class(name, content))

