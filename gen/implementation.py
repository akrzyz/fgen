#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *
from .CppPrinters import *

class ImplementationFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes
        self.__fileName = p_fileName

    def create(self) :
        classes = []
        for _class in self.__classes :
            classes.append(ImplementationFile(_class, self.__fileName))
        return classes

class ImplementationFile:
    def __init__(self, p_class, p_file) :
        self.__class = p_class
        self.__file = p_file
        self.name = createFileName(p_class.namespace(), p_class.name(), CPP)

    def __str__(self) :
        content = functionsImplFromClass(self.__class)
        namespace = self.__class.namespace()
        includes = [self.__file]
        return createSourceFile(content, namespace, includes)

def functionsImplFromClass(p_class) :
    functions = []
    for functionType in p_class.methods() :
        for function in functionType :
            if isFunctionImplNeeded(function) :
                functionImpl = FunctionPrinter(function).implementation(p_class.name())
                functions.append(functionImpl)
    return "\n".join(functions) if len(functions) else ""

def isFunctionImplNeeded(p_function) :
    return p_function.isDeclaration() and \
           not p_function.isPureVirtual()

