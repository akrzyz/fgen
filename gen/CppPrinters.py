#!/usr/bin/env python3
from .consts import *

def GenId():
    x = 1
    while True:
        yield x
        x += 1

class ParamPrinter :
    def __init__(self, param, idGenerator) :
        self._param = param
        self._idGen = idGenerator

    def name(self) :
        if self._param.name() :
            return self._param.name()
        else :
            return "p_param%d" % next(self._idGen)

    def type(self) :
        return self._param.type()

    def typeAndName(self) :
        return self.type() + " " + self.name()

class ParamsPrinter :
    def __init__(self, params) :
        self._params = params

    def types(self) :
        paramsTypes = []
        id = GenId()
        for param in self._params :
            type = ParamPrinter(param, id).type()
            paramsTypes.append(type)
        return ", ".join(paramsTypes);

    def names(self) :
        paramsNames = []
        id = GenId()
        for param in self._params :
            name = ParamPrinter(param, id).name()
            paramsNames.append(name)
        return ", ".join(paramsNames);

    def typesAndNames(self) :
        paramsTypesAndNames = []
        id = GenId()
        for param in self._params :
            typeAndName = ParamPrinter(param, id).typeAndName()
            paramsTypesAndNames.append(typeAndName)
        return ", ".join(paramsTypesAndNames);

class FunctionPrinter :
    def __init__(self, function) :
        self._function = function

    #Function declaration
    def declaration(self) :
        paramsStr = ParamsPrinter(self._function.parameters()).types()
        return self._functionDeclarationBase(paramsStr)

    def declarationWithParamsNames(self) :
        paramsStr = ParamsPrinter(self._function.parameters()).typesAndNames()
        return self._functionDeclarationBase(paramsStr)

    def _functionDeclarationBase(self, paramsStr) :
        ret = ""
        if self._function.isVirtual() : ret += VIRTUAL + " "
        if self._function.isStatic()  : ret += STATIC + " "
        ret += self._functionReturnTypeNameAndParams(paramsStr)
        if self._function.isConst() : ret += " " + CONST
        return ret

    def _functionReturnTypeNameAndParams(self, paramsStr) :
        ret =  self._returnType()
        ret += self._function.name()
        ret += "(%s)" % paramsStr
        return ret

    def _returnType(self) :
        ret = ""
        if self._function.returnType() and not self._function.isConstructor() :
            ret += "%s " % self._function.returnType()
        return ret

    def interface(self) :
        paramsStr = ParamsPrinter(self._function.parameters()).types()
        ret = VIRTUAL + " "
        if self._function.isStatic()  : ret += STATIC + " "
        if self._function.returnType() and not self._function.isConstructor() :
            ret += "%s " % self._function.returnType()
        ret += self._function.name()
        ret += "(%s)" % paramsStr
        if self._function.isConst() : ret += " " + CONST
        ret += " = 0;\n"
        return ret

    def override(self) :
        paramsStr = ParamsPrinter(self._function.parameters()).types()
        ret = self._functionReturnTypeNameAndParams(paramsStr)
        if self._function.isConst() : ret += " " + CONST
        ret += " " + OVERRIDE + ";\n"
        return ret

    #function call
    def call(self):
        functionName = self._function.name()
        functionParamsNames = ParamsPrinter(self._function.parameters()).names()
        return "%s(%s);" % (functionName, functionParamsNames)

    #function implementation
    def implementation(self, className, implementationBody="") :
        if self._function.isConstructor() :
            sygnature =  self._constructodImpl()
        elif self._function.isDestructor() :
            sygnature = self._destructorImpl()
        else :
            sygnature = self._regularFunctionImpl(className)
        return sygnature + "\n" + self._implementationBody(implementationBody)

    def _regularFunctionImpl(self, className) :
        ret = ""
        if self._function.isStatic()  : ret += STATIC + " "
        if self._function.returnType() : ret += "%s " % self._function.returnType()
        ret += "%s::%s" % (className, self._function.name())
        ret += "(%s)" % ParamsPrinter(self._function.parameters()).typesAndNames()
        if self._function.isConst() : ret += " " + CONST
        return ret

    def _constructodImpl(self) :
        className = self._function.name()
        parameters = ParamsPrinter(self._function.parameters()).typesAndNames()
        return "%s::%s(%s)" % (className, className, parameters)

    def _destructorImpl(self) :
        className = self._function.name()
        return "%s::~%s()" % (className, className)

    def _implementationBody(self, body) :
        return "{\n" + body + "}\n"

