#!/usr/bin/env python3
import datetime
from .consts import *

def fileHeader(p_module):
    _dict = {"module": p_module, "year": datetime.datetime.now().strftime("%Y")}

    ret =  '/*********************************************************************************\n'
    ret += '* @file                  $HeadURL:$\n'
    ret += '* @version               $LastChangedRevision:$\n'
    ret += '* @date                  $LastChangedDate:$\n'
    ret += '* @author                $Author:$\n'
    ret += '*\n'
    ret += '* @module                %(module)s\n' % _dict
    ret += '* @owner                 LTE Control Plane\n'
    ret += '*\n'
    ret += '* Copyright %(year)s Nokia. All rights reserved.\n' % _dict
    ret += '**********************************************************************************/\n'
    return ret

def wrapNamespaces(p_namespace, p_string) :
    namespaces = p_namespace.strip("::").split("::")
    if(namespaces[0] == "") : return p_string
    ret ="";
    for namespace in namespaces :
        ret += "%s %s\n" % (NAMESPACE, namespace)
        ret += "{\n"
    ret += "\n%s\n" % p_string
    for namespace in reversed(namespaces) :
        ret += "} //%s %s\n" % (NAMESPACE, namespace)
    return ret;

def headerGuard() :
    return "#pragma once\n"

def globalInclude(p_filename) :
    return "#include <%s>\n" % p_filename

def localInclude(p_filename) :
    return "#include \"%s\"\n" % p_filename

def usingNamespace(p_namespace) :
    return "using %s %s;\n" % (NAMESPACE, p_namespace)

def virtualDestructor(p_className) :
    return VIRTUAL + " ~%s() {}\n" % p_className

def joinNamespaces(p_lNamespace, p_rNamespace):
    namespace = p_lNamespace.rstrip("::").split("::")
    namespace += p_rNamespace.strip("::").split("::")
    return "::".join(namespace).rstrip("::")

def createFileName(p_namespace, p_className, p_fileType):
    return p_namespace.replace("::","") + p_className + "." + p_fileType

def createSourceFile(p_content, p_namespace = "", loacl_includes = [], global_includes = []) :
    return __createFile(p_content, p_namespace, loacl_includes, global_includes, False)

def createHeaderFile(p_content, p_namespace = "", loacl_includes = [], global_includes = []) :
    return __createFile(p_content, p_namespace, loacl_includes, global_includes, True)

def __createFile(p_content, p_namespace, loacl_includes, global_includes, p_isHeaderGuardNeeded) :
    component = p_namespace.strip("::").split("::")[0]
    ret = fileHeader(str(component))
    ret+= "\n"
    if p_isHeaderGuardNeeded : ret += headerGuard() + "\n"
    for include in global_includes :
        ret+= globalInclude(str(include))
    for include in loacl_includes :
        ret+= localInclude(str(include))
    if loacl_includes : ret += "\n"
    ret += wrapNamespaces(p_namespace, str(p_content)) + "\n"
    return ret

class Class :
    def __init__ (self, p_name, p_content = "", p_inheritances = {}) :
        self.name = p_name
        self.content = p_content
        self.inheritances = p_inheritances

    def getIhneritesList(self) :
        inheritances = self.createInheritanceList()
        if inheritances :
            return " : " + inheritances
        return ""

    def createInheritanceList(self) :
        inheritances = []
        for inheritanceType, inheritancesTypeList in self.inheritances.items() :
            for inheritance in inheritancesTypeList :
                inheritances.append(inheritanceType + " " + inheritance)
        return ", ".join(inheritances)

    def __str__(self):
        ret  = CLASS + " " + self.name + self.getIhneritesList() + "\n"
        ret += "{\n"
        ret += self.content
        ret += "};\n"
        return ret

