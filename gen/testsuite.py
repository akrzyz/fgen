#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *

class TestSuiteFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes
        self.__fileName = p_fileName

    def create(self) :
        classes = []
        for _class in self.__classes :
            classes.append(TestSuiteFile(_class, self.__fileName))
        return classes

class TestSuiteFile:
    def __init__(self, p_class, p_fileName) :
        self.__class = p_class
        self.__file = p_fileName
        self.__suiteName = removeILikieInterface(p_class.name()) + TEST_SUITE_NAME_POSTFIX
        self.name = createFileName(p_class.namespace(), self.__suiteName, CPP)

    def __str__(self) :
        content = testSuiteBody(self.__suiteName)
        namespace = joinNamespaces(self.__class.namespace(), UT_NESTED_NAMESPACE)
        local_includes = [self.__file]
        global_includes = [GTEST_HEADER]
        return createSourceFile(content, namespace, local_includes, global_includes)

def testSuiteBody(p_suiteName) :
    ret  = usingNamespace(GTEST_NAMESPACE);
    ret += "\n"
    ret += testSuiteClass(p_suiteName)
    ret += "\n"
    ret += testSuiteTestDummy(p_suiteName)
    return ret

def testSuiteClass(p_suiteName) :
    inheritances = {ACCESS_PUBLIC : [GTEST_SUITE_BASE_CLASS]}
    return str(Class(p_suiteName,"",inheritances))

def testSuiteTestDummy(p_suiteName) :
    ret  = "TEST_F(%s, TestDummy)\n" % p_suiteName
    ret += "{\n"
    ret += INDENT + "EXPECT_TRUE(false);\n"
    ret += "}\n"
    return ret

