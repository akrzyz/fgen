#!/usr/bin/env python3
from .commons import *
from .utils import *
from .CppPrinters import *
from .consts import *
import re

def MockedMethod(p_method):
    if __isOperator(p_method.name()) :
        return str(__OperatorMock(p_method))
    return str(__RegularMethodMock(p_method))

def __RegularMethodMock(p_method):
    return "    %s(%s, %s(%s));\n" % (__MockMethodMacro(p_method),
                                      p_method.name(),
                                      p_method.returnType(),
                                      ParamsPrinter(p_method.parameters()).types())
def __MockMethodMacro(p_method) :
    ret = "MOCK_"
    if(p_method.isConst()) : ret+=("CONST_")
    ret += ("METHOD")
    ret += (str(len(p_method.parameters())))
    return ret

def __OperatorMock(p_method):
    if p_method.name() in __regularOperatorsNames :
        mock = __RegularOperatorMock(p_method)
    else :
        mock = __ConversionOperatorMock(p_method)

    ret = INDENT + "%s(%s, %s(%s));\n" % (__MockMethodMacro(p_method),
                                          mock[FUNCTION_NAME],
                                          mock[RETURN_TYPE],
                                          ParamsPrinter(mock[PARAMETERS]).types())
    ret += INDENT + FunctionPrinter(p_method).declarationWithParamsNames() + "\n"
    ret += INDENT + "{\n"
    ret += 2 * INDENT + "return %s(%s);\n" % (mock[FUNCTION_NAME], ParamsPrinter(mock[PARAMETERS]).names())
    ret += INDENT + "}\n"
    return ret

def __isOperator(p_name) :
    if re.match(r"^operator", p_name) :
        return True
    return False

def __RegularOperatorMock(p_method):
    return {
            FUNCTION_NAME : __regularOperatorsNames[p_method.name()],
            RETURN_TYPE   : p_method.returnType(),
            PARAMETERS    : p_method.parameters()
           }

__regularOperatorsNames = {
    #Arithmetic operators
    "operator="  : "assignmentOperator",
    "operator+"  : "additionOperator",
    "operator-"  : "subtractionOperator",
    "operator*"  : "multiplicationOperator",
    "operator/"  : "divisionOperator",
    "operator%"  : "moduloOperator",
    "operator++" : "incrementOperator",
    "operator--" : "decrementOperator",
    #Comparison operators
    "operator==" : "equalToOperator",
    "operator!=" : "notEqualToOperator",
    "operator>"  : "greaterThanOperator",
    "operator<"  : "lessThanOperator",
    "operator>=" : "greaterThanOrEqualToOperator",
    "operator<=" : "lessThanOrEqualToOperator",
    #Logical operators
    "operator!"  : "logicalNotOperator",
    "operator&&" : "logicalAndOperator",
    "operator||" : "logicalOrOperator",
    #Bitwise operators
    "operator~"  : "bitwiseNotOperator",
    "operator&"  : "bitwiseAndOperator",
    "operator|"  : "bitwiseOrOperator",
    "operator^"  : "bitwiseXorOperator",
    "operator<<" : "bitwiseLeftSiftOperator",
    "operator>>" : "bitwiseRightShiftOperator",
    #Compound assignment operators
    "operator+=" : "additionAssignmentOperator",
    "operator-=" : "subtractionAssignmentOperator",
    "operator*=" : "multiplicationAssignmentOperator",
    "operator/=" : "divisionAssignmentOperator",
    "operator%=" : "moduloAssignmentOperator",
    "operator&=" : "bitwiseAndAssignmentOperator",
    "operator|=" : "bitwiseOrAssignmentOperator",
    "operator^=" : "bitwiseXorAssignmentOperator",
    "operator<<=" : "bitwiseLeftShiftAssignmentOperator",
    "operator>>=" : "bitwiseRightShiftAssignmentOperator",
    #Member and pointer operators
    "operator[]" : "arraySubscriptOperator",
    "operator*"  : "indirectionOperator",
    "operator&"  : "referenceOperator",
    "operator->" : "structureDereferenceOperator",
    "operator->*" : "memberPointedOperator",
    #Other operators
    "operator()" : "functionCallOperator",
    "operator,"  : "commsOperator",
    #"operator" : "conversionOperator", #parser do it wrong :(
    "operator new" : "newOperator",
    "operator new[]" : "newArrayOperator",
    "operator delete" : "deleteOperator",
    "operator delete[]" : "deleteArrayOperator",
    }

def __ConversionOperatorMock(p_method) :
    mock = {}
    mock[RETURN_TYPE]    = __convertionOperatorRtnType(p_method.name())
    mock[FUNCTION_NAME]  = __convertionOperatorNameStr(mock[RETURN_TYPE])
    mock[PARAMETERS]     = p_method.parameters()
    return mock

def __convertionOperatorNameStr(p_name) :
    conversionTypeName = capitalizeFirstWordLetter(p_name)
    conversionTypeName = conversionTypeName.replace("&", "Ref")
    conversionTypeName = conversionTypeName.replace("*", "Ptr")
    conversionTypeName = conversionTypeName.replace(" ", "")
    name = "conversionTo" + conversionTypeName + "Operator"
    return name

def __convertionOperatorRtnType(p_name) :
    return re.sub("operator( \w.*)", r"\1", p_name).strip()

