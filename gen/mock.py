#!/usr/bin/env python3
from .commons import *
from .utils import *
from .mockMethod import *
from .consts import *
import re

class MockFilesFactory:
    def __init__(self, p_classes, p_fileName) :
        self.__classes = p_classes
        self.__fileName = p_fileName

    def __hasPublicVirtualMethods(self, p_class) :
        for method in p_class.publicMethods() :
            if method.isVirtual():
                return True
        return False

    def create(self) :
        classes = []
        for _class in self.__classes :
            if self.__hasPublicVirtualMethods(_class) :
                classes.append(MockFile(_class, self.__fileName))
        return classes

class MockFile:
    def __init__(self, p_class, p_fileName) :
        self.__class = p_class
        self.__fileName = p_fileName
        self.mockClass = MockClass(p_class)
        self.mockedClass = MockedClass(p_class)
        self.name = createFileName(p_class.namespace(), self.mockClass.name, HPP)

    def __str__(self) :
        content = str(self.mockClass)
        namespace = joinNamespaces(self.__class.namespace() , UT_NESTED_NAMESPACE)
        local_includes = [self.__fileName]
        global_includes = [GMOCK_HEADER]
        return createHeaderFile(content, namespace, local_includes, global_includes)

class MockClass:
    def __init__(self, p_class) :
        self.__class = p_class
        self.name = self.createMockName()
        self.fullName = self.name
        self.joinNemspecesToFullName()

    def createMockName(self) :
        return removeILikieInterface(self.__class.name()) + MOCK_NAME_POSTFIX;

    #TODO WTF is that?
    def joinNemspecesToFullName(self) :
        if self.__class.namespace() : self.fullName = joinNamespaces(self.__class.namespace() , self.fullName)

    def getMethods(self) :
        ret = ACCESS_PUBLIC + ":\n"
        for method in self.__class.publicMethods() :
            if(shouldBeMocked(method)) :
                ret += str(MockedMethod(method))
        return ret

    def __str__(self) :
        className = self.name
        content = self.getMethods()
        inheritances = {ACCESS_PUBLIC : [self.__class.name()]}
        return str(Class(className, content, inheritances))

def shouldBeMocked(p_method) :
    return p_method.isVirtual() and not p_method.isDestructor()

class MockedClass:
    def __init__(self, p_class) :
        self.name = p_class.name()
        self.fullName = self.name
        if p_class.namespace() : self.fullName = joinNamespaces (p_class.namespace() , self.fullName)

