#!/usr/bin/env python3
import sys
import os
from .print_utils import *
from .parser_utils import *

def addIndent(string, indent) :
    lines = []
    for line in string.splitlines(True) :
        lines.append(indent + line)
    return "".join(lines)

def boolQuestion(text) :
    userInput = ""
    while True :
        userInput = input(text).strip().upper()
        if userInput in ("Y","N") :
            break
    if userInput == "Y" :
            return True
    return False

def capitalizeFirstWordLetter(s) :
    words = s.strip().split(" ")
    for i in range(len(words)) :
        word = list(words[i])
        word[0] = word[0].upper()
        words[i] = "".join(word)
    return " ".join(words)

def removeILikieInterface(p_name) :
    if(len(p_name) > 1 and p_name[0] == "I" and p_name[1].isupper()) :
        return p_name[1:]
    return p_name

def addILikieInterface(p_name) :
    if(len(p_name) > 1 and p_name[0] == "I" and p_name[1].isupper()) :
        return p_name
    else :
        return "I" + p_name

def writeToFile(p_file, p_content, p_mode="w") :
    try:
        f = open(p_file, p_mode)
        try:
            f.write(p_content)
        finally:
            f.close()
    except IOError as e:
        WARN(e)
        sys.exit(1)

def doYouWantToCreateDirectoryIfNotExist(p_dir, p_force = False) :
    if not os.path.exists(p_dir) :
         if p_force or boolQuestion("directory: \"" + str(p_dir) + "\" not exist, do you wan to create it? Y/N: ") :
             os.makedirs(p_dir)
         else :
            sys.exit(1)

def doYouWantToOverwriteFileIfExist(p_file, p_force = False) :
    if os.path.exists(p_file) :
        if (p_force or boolQuestion("file: \"" + str(p_file) + "\" already exist, do you wan to overwrite it? Y/N: ")) :
            return True
        else :
            return False
    else :
        return True

####################################
###### GEN REFACTORING STUFFS ######
####################################

class Checker :
    def __init__(self) :
        self._checks = []

    def add_check(self, check) :
        self._checks.append(check)

    def check(self, obj) :
        for check in self._checks :
            if not check(obj) :
                return False
        return True

def checkAtLeastOneArg(options):
    if not options.args :
        WARN("program require at least one input argument")
        return False
    return True

def checkFileFromArgsExist(options):
    for filePath in options.args :
        if not os.path.isfile(filePath) :
            WARN("file: \"" + filePath + "\" not exist")
            return False
    return True

def checkAtLeastOneInterface(options):
    if options.interfaces :
            return True
    WARN("program require at least one input class name")
    return False

def Generate(fileGenerator, generatorInput, outputDirectory, force=False) :
        doYouWantToCreateDirectoryIfNotExist(outputDirectory, force)
        generatedFilesNames = []
        for elem in generatorInput :
            generatedFilesNames += fileGenerator.generate(elem, outputDirectory, force)
        return generatedFilesNames

class FileGenerator :
    def __init__ (self, fileFactory) :
        self.fileFactory = fileFactory

    def generate(self, inputFile, outputDirectory, force) :
        inputFileName = getFileName(inputFile)
        parsedCppHeader = parseHppFile(inputFile)
        createdFiles = self.fileFactory(parsedCppHeader.classes(), inputFileName).create()
        savedFiles = saveFiles(outputDirectory, createdFiles, force)
        return savedFiles

def getFileName(filePath) :
    return os.path.split(filePath)[1]

def saveFiles(outputDir, files, force=False):
    savedFiles = []
    for fileToSave in files :
        outputFile = os.path.join(outputDir, fileToSave.name)
        if doYouWantToOverwriteFileIfExist(outputFile, force) :
            writeToFile(outputFile, str(fileToSave))
            savedFiles.append(outputFile)
            LOG("file " + outputFile + " was generated")
    return savedFiles

