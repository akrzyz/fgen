#!/usr/bin/env python3
from .commons import *
from .utils import *
from .consts import *

class InterfaceFile:
    def __init__(self, p_fullName) :
        namespaces = p_fullName.split("::")
        self.__namespace = "::".join(namespaces[:-1])
        self.__className = namespaces[-1]
        self.name =  createFileName(p_fullName,"",HPP)

    def __str__(self) :
        namespace = self.__namespace
        content = interfaceClass(self.__className)
        return createHeaderFile(content, namespace)

def interfaceClass(p_name) :
    content =  ACCESS_PUBLIC + ":\n"
    content += INDENT + virtualDestructor(p_name)
    return str(Class(p_name, content))

