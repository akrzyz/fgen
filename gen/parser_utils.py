#!/usr/bin/env python3
import sys
from .print_utils import *
import re
from .CppHeaderParser import *
from .CppHeaderParserWrappers import CppHeader
from .consts import *

def parseHppFileNotWrapped(p_file) :
    try:
        orginalCppHeader = fix(CppHeaderParser.CppHeader(p_file))
        return orginalCppHeader
    except CppHeaderParser.CppParseError as e:
        WARN(e)
        sys.exit(1)

def parseHppFile(p_file) :
    try:
        orginalCppHeader = fix(CppHeaderParser.CppHeader(p_file))
        return CppHeader(orginalCppHeader)
    except CppHeaderParser.CppParseError as e:
        WARN(e)
        sys.exit(1)

def fix(p_cppHeader):
    for _class in p_cppHeader.classes.values() :
        for methods in _class[METHODS].values() :
            for method in methods :
                fixFunctions(method)
    return p_cppHeader

def fixFunctions(p_method) :
    fixOperator(p_method)
    fixMarkOverridedFunctionAsVirtual(p_method)
    for param in p_method[PARAMETERS] :
        fixParam(param)

def fixOperator(p_method) :
    #operator new, delte and conversion fix
    operatorName = re.sub(r"^(operator)(\w+.*)", r"\1 \2", p_method[NAME])
    #operator conversion fix
    operatorName = re.sub(r"(static)", r"\1 ", operatorName)
    operatorName = re.sub(r"(const)", r"\1 ", operatorName)
    operatorName = re.sub(r"\*", r" *", operatorName)
    operatorName = re.sub(r"&", r" &", operatorName)
    p_method[NAME] = operatorName
    fixConvertionOperator(p_method)

def fixConvertionOperator(p_method):
    #is convertion operator
    if(re.match(r"operator \w+", p_method[NAME]) and
       not re.match(r"operator new", p_method[NAME]) and
       not re.match(r"operator delete", p_method[NAME])) :
        p_method[RETURN_TYPE] = ""

def fixMarkOverridedFunctionAsVirtual(p_function):
    if re.search(r" override ", p_function[DEBUG]) :
        p_function[VIRTUAL] = True

def fixParam(p_param) :
    if p_param[TYPE] in (CONST,STATIC) :
        p_param[TYPE] += " " + p_param[NAME]
        p_param[NAME] = ""
    if p_param[NAME] in ("&","*") :
        p_param[TYPE] += " " + p_param[NAME]
        p_param[NAME] = ""

