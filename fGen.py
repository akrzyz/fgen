#!/usr/bin/env python3
from optparse import OptionParser
from gen.utils import *
from gen.mock import *
from gen.consts import *
import factoryGen
import headerGen
import implementationGen
import interfaceGen
import mockGen
import testsuiteGen
import extractInterfacesGen

def Options() :
    usage = "usage: %prog [OPTIONS]... FILE..."
    epilog  = "example: fGen.py -ia Common::Foo::IBar will generate: "
    epilog += DEFAULT_OUTPUT_DIR_INTERFACE + "./CommonFooIBar.hpp, "
    epilog += DEFAULT_OUTPUT_DIR_HEADER + "./CommonFooBar.hpp, "
    epilog += DEFAULT_OUTPUT_DIR_IMPLEMENTATION + "./CommonFooBar.cpp, "
    epilog += DEFAULT_OUTPUT_DIR_TEST_SUITE + "./CommonFooBarTestSuite.cpp, "
    epilog += DEFAULT_OUTPUT_DIR_MOCK + "./CommonFooBarMock.hpp"
    epilog += "                             "
    epilog += "example2: fGen.py -a CommonFooIBar.h will generate: "
    epilog += DEFAULT_OUTPUT_DIR_HEADER + "./CommonFooBar.hpp, "
    epilog += DEFAULT_OUTPUT_DIR_IMPLEMENTATION + "./CommonFooBar.cpp, "
    epilog += DEFAULT_OUTPUT_DIR_TEST_SUITE + "./CommonFooBarTestSuite.cpp, "
    epilog += DEFAULT_OUTPUT_DIR_MOCK + "./CommonFooBarMock.hpp"

    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option("-i", "--interface",         dest="gen_interface",            action="store_true", default=False, help="Generate interface file")
    parser.add_option("-e", "--extract_interface", dest="gen_extracted_interfaces", action="store_true", default=False, help="Extract interface classes from header file")
    parser.add_option("-m", "--mock",              dest="gen_mock",                 action="store_true", default=False, help="Generate mock file")
    parser.add_option("-p", "--header",            dest="gen_header",               action="store_true", default=False, help="Generate header file") #-h is reserved fot help :(
    parser.add_option("-c", "--cpp",               dest="gen_impl",                 action="store_true", default=False, help="Generate implementation file.")
    parser.add_option("-t", "--test_suite",        dest="gen_test_suite",           action="store_true", default=False, help="Generate test suite file")
    parser.add_option("-b", "--factory",           dest="gen_factory",              action="store_true", default=False, help="Generate factory files - interface/header/cpp") #-f is force
    parser.add_option("-a", "--all"    ,           dest="gen_all",                  action="store_true", default=False, help="like -pcmt")
    parser.add_option("-A", "--ALL"    ,           dest="gen_ALL",                  action="store_true", default=False, help="like -pcmtf")
    parser.add_option("-f", "--force",             dest="force",                    action="store_true", default=False, help="force file overwrite and directory creation.")
    parser.add_option("-d", "--output_dir",        dest="output_dir",                                    default=None,  help="Output directory for generated files")
    (options, args) = parser.parse_args()
    options.args = args

    if options.gen_all :
        handleOption_all(options)
    if options.gen_ALL :
        handleOption_ALL(options)

    return options

def handleOption_all(options) :
    options.gen_header = True
    options.gen_impl = True
    options.gen_test_suite = True
    options.gen_mock = True

def handleOption_ALL(options) :
    handleOption_all(options)
    options.gen_factory = True

def OptionsConsistencyChecker(options) :
    checker = Checker()
    checker.add_check(checkAtLeastOneArg)
    checker.add_check(checkInterfaceXorExtractInterface)
    if not options.gen_interface :
       checker.add_check(checkFileFromArgsExist)
    return checker

def checkInterfaceXorExtractInterface(options) :
    if options.gen_interface and options.gen_extracted_interfaces :
        WARN("options -i and -e are mutually exclusive")
        return False
    return True

class GlobalGenerator :
    def __init__(self, options) :
        self.options = options
        self.force = options.force
        self.setOutputDirs()

    def setOutputDirs(self) :
        if self.options.output_dir :
            self.interface_dir = self.options.output_dir
            self.header_dir = self.options.output_dir
            self.impl_dir = self.options.output_dir
            self.test_suite_dir = self.options.output_dir
            self.mock_dir = self.options.output_dir
            self.factory_interdace_dir = self.options.output_dir
        else :
            self.interface_dir = DEFAULT_OUTPUT_DIR_INTERFACE
            self.header_dir = DEFAULT_OUTPUT_DIR_HEADER
            self.impl_dir = DEFAULT_OUTPUT_DIR_IMPLEMENTATION
            self.test_suite_dir = DEFAULT_OUTPUT_DIR_TEST_SUITE
            self.mock_dir = DEFAULT_OUTPUT_DIR_MOCK
            self.factory_interdace_dir = DEFAULT_OUTPUT_DIR_FACTORY_INTERFACE

    def generate(self) :
        interfaceFiles = self.generateInterfaces(self.options.args)
        mockFiles      = self.generateMocks(interfaceFiles)
        factoryFiles   = self.generateFactories(interfaceFiles)
        headerFiles    = self.generateHeaders(interfaceFiles)
        cppFiles       = self.generateImplementations(headerFiles)
        testSuiteFiles = self.generateTestSuites(headerFiles)

    def generateInterfaces(self, generatorInput) :
        if self.options.gen_interface :
            return interfaceGen.generate(generatorInput, self.interface_dir, self.force)
        return generatorInput

    def generateExtractedInterfaces(self, generatorInput) :
        if self.options.gen_extracted_interfaces :
            return extractInterfacesGen.generate(generatorInput, self.interface_dir, self.force)
        return generatorInput

    def generateHeaders(self, generatorInput) :
        if self.options.gen_header :
            return headerGen.generate(generatorInput, self.header_dir, self.force)
        return generatorInput

    def generateImplementations(self, generatorInput) :
        if self.options.gen_impl :
            return implementationGen.generate(generatorInput, self.impl_dir, self.force)
        return generatorInput

    def generateTestSuites(self, generatorInput) :
        if self.options.gen_test_suite :
            return testsuiteGen.generate(generatorInput, self.test_suite_dir, self.force)
        return generatorInput

    def generateMocks(self, generatorInput) :
        if self.options.gen_mock :
            return mockGen.generate(generatorInput, self.mock_dir, self.force)
        return generatorInput

    def generateFactories(self, generatorInput) :
        if self.options.gen_factory :
            return factoryGen.generate(generatorInput, self.factory_interdace_dir, self.force)
        return generatorInput

def run(options, checker) :
    if checker.check(options) :
        return generate(options)

def generate(options) :
    return GlobalGenerator(options).generate()

if __name__ == "__main__":
    options = Options()
    checker = OptionsConsistencyChecker(options)
    run(options, checker)

