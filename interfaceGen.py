#!/usr/bin/env python3
from optparse import OptionParser
from gen.interface import *
from gen.utils import *
from gen.consts import *

def Options() :
    usage = "usage: %prog [OPTIONS]... CLASS_FULL_NAME..."
    epilog = "example: %prog Aaa::Bbb::IFoo file AaaBbbIFoo.hpp will be generated in current directory."
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option("-f", "--force", dest="force", action="store_true", default=False, help="Force file overwrite.")
    parser.add_option("-d", "--output_dir", dest="output_dir", default=DEFAULT_OUTPUT_DIR_INTERFACE, help="File output directory, by default is: " + DEFAULT_OUTPUT_DIR_INTERFACE)
    (options, args) = parser.parse_args()
    options.args = args
    return options

def OptionsConsistencyChecker() :
    checker = Checker()
    checker.add_check(checkAtLeastOneArg)
    return checker

class InterfaceFileGenerator :
    def generate (self, classFullName, outputDir, force) :
        interfaceFile = InterfaceFile(classFullName)
        return saveFiles(outputDir, [interfaceFile], force)

def run(options, checker) :
    if checker.check(options) :
        inputClassesNames = options.args
        return generate(inputClassesNames, options.output_dir, options.force)

def generate(inputClassesNames, outputDirectory, force) :
    interfaceFileGenerator = InterfaceFileGenerator()
    return Generate(interfaceFileGenerator, inputClassesNames, outputDirectory, force)

if __name__ == "__main__":
    options = Options()
    checker = OptionsConsistencyChecker()
    run(options, checker)

