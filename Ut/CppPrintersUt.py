#!/usr/bin/env python3
import unittest
from CppWrappersMocks import *
from gen.CppPrinters import *

class CppPrintersTestCase(unittest.TestCase):

    def setUp(self):
        self.classNameBar = "Bar"
        self.paramWithNameMock = copy.deepcopy(parameterStrictMock)
        self.paramWithNameMock.type = Mock(return_value = "int")
        self.paramWithNameMock.name = Mock(return_value = "p_id")

        self.paramWithOutNameMock = copy.deepcopy(parameterStrictMock)
        self.paramWithOutNameMock.type = Mock(return_value = "const float &")
        self.paramWithOutNameMock.name = Mock(return_value = "")

        self.functionMock = copy.deepcopy(functionDefaultMock)
        self.functionMock.isVirtual.return_value = True
        self.functionMock.isConst.return_value = True

        self.constructorMock = copy.deepcopy(constructorDefaultMock)
        self.constructorMock.parameters.return_value = [self.paramWithNameMock]

        self.destructorMock = copy.deepcopy(destructorDefaultMock)
        self.id = GenId()

    def testPrintParamName_ParamWithName(self):
        printer = ParamPrinter(self.paramWithNameMock, self.id)
        self.assertEqual("p_id", printer.name())

    def testPrintParamName_ParamWithOutName(self):
        printer = ParamPrinter(self.paramWithOutNameMock, self.id)
        self.assertEqual("p_param1", printer.name())

    def testPrintParamNameAndType_ParamWithName(self):
        printer = ParamPrinter(self.paramWithNameMock, self.id)
        self.assertEqual("int p_id",printer.typeAndName())

    def testPrintParamNameAndType_ParamWithOutName(self):
        printer = ParamPrinter(self.paramWithOutNameMock, self.id)
        self.assertEqual("const float & p_param1", printer.typeAndName())

    def testPrintParamsTypes(self):
        printer = ParamsPrinter([self.paramWithNameMock, self.paramWithOutNameMock])
        self.assertEqual("int, const float &", printer.types())

    def testPrintParamsNames(self):
        printer = ParamsPrinter([self.paramWithNameMock, self.paramWithOutNameMock])
        self.assertEqual("p_id, p_param1", printer.names())

    def testPrintParamsTypesAndNames(self):
        printer = ParamsPrinter([self.paramWithNameMock, self.paramWithOutNameMock])
        self.assertEqual("int p_id, const float & p_param1", printer.typesAndNames())

    def testPrintFunctionDeclaration_NoParams(self):
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("virtual void foo() const", printer.declaration())

    def testPrintFunctionDeclaration_TwoParams(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("virtual void foo(int, const float &) const", printer.declaration())

    def testPrintConstructorDeclaration(self):
        printer = FunctionPrinter(self.constructorMock)
        self.assertEqual("Bar(int)", printer.declaration())

    def testPrintFunctionDeclarationWithParamsNames_TwoParams(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("virtual void foo(int p_id, const float & p_param1) const", printer.declarationWithParamsNames())

    def testPrintConstructorDeclarationWithParamsNames(self):
        printer = FunctionPrinter(self.constructorMock)
        self.assertEqual("Bar(int p_id)", printer.declarationWithParamsNames())

    def testFunctionCall2str(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("foo(p_id, p_param1);", printer.call())

    def testRegularFunctionImplementation2strWithFunctionBody(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock]
        printer = FunctionPrinter(self.functionMock)
        body = "    dummy();\n"
        s  = "void Bar::foo(int p_id) const\n"
        s += "{\n"
        s += body
        s += "}\n"
        self.assertEqual(s, printer.implementation(self.classNameBar, body))

    def testRegularFunctionImplementation2strNoParamName(self):
        self.functionMock.parameters.return_value = [self.paramWithOutNameMock]
        printer = FunctionPrinter(self.functionMock)
        s  = "void Bar::foo(const float & p_param1) const\n"
        s += "{\n"
        s += "}\n"
        self.assertEqual(s, printer.implementation(self.classNameBar))

    def testConstructodImplementationf2str(self):
        printer = FunctionPrinter(self.constructorMock)
        s  = "Bar::Bar(int p_id)\n"
        s += "{\n"
        s += "}\n"
        self.assertEqual(s, printer.implementation(self.classNameBar))

    def testDestructorImplementationf2str(self):
        printer = FunctionPrinter(self.destructorMock)
        s  = "Bar::~Bar()\n"
        s += "{\n"
        s += "}\n"
        self.assertEqual(s, printer.implementation(self.classNameBar))

    def testPrintFunctionInterface(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        self.functionMock.isVirtual.return_value = False
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("virtual void foo(int, const float &) const = 0;\n", printer.interface())

    def testPrintFunctionOverride(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        self.functionMock.isVirtual.return_value = False
        self.functionMock.isConst.return_value = False
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("void foo(int, const float &) override;\n", printer.override())

    def testPrintConstFunctionOverride(self):
        self.functionMock.parameters.return_value = [self.paramWithNameMock, self.paramWithOutNameMock]
        self.functionMock.isVirtual.return_value = True
        self.functionMock.isConst.return_value = True
        printer = FunctionPrinter(self.functionMock)
        self.assertEqual("void foo(int, const float &) const override;\n", printer.override())

if __name__ == "__main__":
    unittest.main()

