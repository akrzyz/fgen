#include <string>

namespace Enbc
{
namespace LoadBalancing
{

struct Client
{
    virtual void setup(int);
    virtual void setup(int *);
    virtual void setup2(int p_id);
    virtual bool start(const std::string & , int ) const;
    virtual bool start(const std::string & p_name) const;
    virtual bool start(const char * , int p_id ) const;
    virtual bool stop();
    void dumy();
    private:
    void dumyPrivat();
};

struct QQQ
{
    virtual void foo();
    struct InerQQQ;
};

struct IntraFreqCell
{
    virtual void foo();
};

struct Ttt
{
    virtual ~Ttt(){};
};

struct IAaa
{
    virtual void abstractFoo() = 0;
    struct InerIAAA
    {
        virtual void asd();  
    };
};

struct NoVirtual : public IAaa
{
    NoVirtual(int);
    void abstractFoo();
};

} //namespace Enbc
} //namespace LoadBalancing
