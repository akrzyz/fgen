/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef IQWEFACTORY_HPP
#define IQWEFACTORY_HPP

#include <boost/shared_ptr.hpp>
#include <qwe.hpp>

struct IQweFactory
{
    ~IQweFactory(){}
    boost::shared_ptr<Qwe> create() = 0;
};

#endif //IQWEFACTORY_HPP

