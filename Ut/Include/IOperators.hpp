
struct IMyClass
{
    virtual IBcchModIndReqWrapper& operator += (const EPagingIndication p_paging);
    virtual bool operator<(const MessageIdentifier& p_rhs) const;
    virtual bool operator()(const u16 p_warningMsgId) const;
    virtual bool operator()(const u16, int, const IAa &) const;
    virtual bool operator()(const u16, int p_id, const IAa *, const IQq & p_iq) const;
//    virtual operator R ();
    virtual void * operator new(size T);
    virtual void * operator new[](size T);
    virtual operator ToType();
    virtual operator const ToType();
    virtual operator const ToType & ();
    virtual operator ToType * ();
    virtual operator int ();
//    virtual operator const ToType * ();
};
