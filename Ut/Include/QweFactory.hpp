/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef QWEFACTORY_HPP
#define QWEFACTORY_HPP

#include <Ut/Include/IQweFactory.hpp>

struct QweFactory : public IQweFactory
{
    boost::shared_ptr<Qwe> create();
};

#endif //QWEFACTORY_HPP

