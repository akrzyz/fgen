/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef AAAQWEFACTORY_HPP
#define AAAQWEFACTORY_HPP

#include <AaaIQweFactory.hpp>

namespace Aaa
{

struct QweFactory : public IQweFactory
{
    boost::shared_ptr<Qwe> create();
};

} //namespace Aaa

#endif //AAAQWEFACTORY_HPP

