/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef AAAIQWEFACTORY_HPP
#define AAAIQWEFACTORY_HPP

#include <boost/shared_ptr.hpp>
#include <qwe.hpp>

namespace Aaa
{

struct IQweFactory
{
    ~IQweFactory(){}
    boost::shared_ptr<Qwe> create() = 0;
};

} //namespace Aaa

#endif //AAAIQWEFACTORY_HPP

