#!/usr/bin/env python3
import os
import unittest
import datetime
from mock import patch, MagicMock
from gen import parser_utils
import interfaceGen
import headerGen
import implementationGen
import factoryGen
import mockGen
import testsuiteGen
import extractInterfacesGen
import subprocess
import fGen

class DirectorySetter :
    def __init__(self):
        self._base = self.getBaseDir()
        self.base = os.path.join(self._base, "BaseFiles")
        self.ref  = os.path.join(self._base, "ReferenceFiles")
        self.gen  = os.path.join(self._base, "GeneratedFiles")
        self.assertDirs()
        self.clearGenDirectory()
        self.clearFGenDirectory()

    def getBaseDir(self) :
        if os.path.exists("AcceptanceTests.py") :
            baseDir = "."
        elif os.path.exists(os.path.join("AcceptanceTests","acceptanceTests.py")) :
            baseDir = os.path.join(".","AcceptanceTests")
        elif os.path.exists(os.path.join("Ut","AcceptanceTests","acceptanceTests.py")) :
            baseDir = os.path.join(".","Ut","AcceptanceTests")
        else :
            raise EnvironmentError("Wrong direcotry to start AcceptanceTests")
        return baseDir

    def assertDirs(self) :
        assert os.path.exists(self._base), "directory not exist!: " + self._base
        assert os.path.exists(self.base),   "directory not exist!: " + self.base
        assert os.path.exists(self.ref),    "directory not exist!: " + self.ref
        assert os.path.exists(self.gen),    "directory not exist!: " + self.gen

    def clearGenDirectory(self) :
        print("removing old generated files from : " + self.gen)
        files = os.listdir(self.gen)
        for file in files :
            dirToRemove = os.path.join(self.gen, file)
            if os.path.isfile(dirToRemove) :
                os.remove(dirToRemove)
                print("removing: " + dirToRemove)

    def clearFGenDirectory(self) :
        fGenGenerateDir = os.path.join(self._base, "fGenFiles","Generated")
        print("removing old generated files from : " + fGenGenerateDir)
        files = os.listdir(fGenGenerateDir)
        for file in files :
            dirToRemove = os.path.join(fGenGenerateDir, file)
            if os.path.isfile(dirToRemove) :
                os.remove(dirToRemove)
                print("removing: " + dirToRemove)

dirs = DirectorySetter()

timeNowMock = MagicMock()
timeNowMock.strftime = MagicMock(return_value = "2014")

#immutability hack for datetime
class NewDate(datetime.datetime):
    @classmethod
    def now(arg):
        return timeNowMock
datetime.datetime = NewDate

class AcctepanceTestsTestCase(unittest.TestCase) :

    def cmpFiles(self, p_expectedFile, p_fileToCmp) :
        expectedFile = open(p_expectedFile, "r")
        file2cmpFile = open(p_fileToCmp, "r")
        try :
            self.maxDiff = None
            errMsg = "file %s != %s" % (p_expectedFile, p_fileToCmp)
            self.assertEqual(expectedFile.read(), file2cmpFile.read(), errMsg)
        finally :
            expectedFile.close()
            file2cmpFile.close()

    def testGenerateInterface(self) :
        options = interfaceGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = ["Aaa::Bbb::IFoo", "Ccc::Ddd::Bra"]

        checker = interfaceGen.OptionsConsistencyChecker()
        interfaceGen.run(options, checker)

        refFile = os.path.join(dirs.ref, "AaaBbbIFoo.hpp");
        genFile = os.path.join(dirs.gen, "AaaBbbIFoo.hpp");
        self.cmpFiles(refFile, genFile)

        refFile = os.path.join(dirs.ref, "CccDddBra.hpp");
        genFile = os.path.join(dirs.gen, "CccDddBra.hpp");
        self.cmpFiles(refFile, genFile)

    def testGenHeaderVirtualDestOnly(self) :
        baseVirtualDestructorOnly      = os.path.join(dirs.base, "GenHeaderIVirtualDestOnly.hpp")
        expectedVirtualDestructorOnly  = os.path.join(dirs.ref,  "GenHeaderVirtualDestOnly.hpp")
        generatedVirtualDestructorOnly = os.path.join(dirs.gen,  "GenHeaderVirtualDestOnly.hpp")

        options = headerGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseVirtualDestructorOnly]

        checker = headerGen.OptionsConsistencyChecker()
        headerGen.run(options,checker)

        self.cmpFiles(expectedVirtualDestructorOnly, generatedVirtualDestructorOnly)

    def testGenHeaderTwoFiles(self) :
        baseFileIFoo     = os.path.join(dirs.base, "GenHeaderTwoFilesIFoo.hpp")
        expectedFileFoo  = os.path.join(dirs.ref,  "GenHeaderTwoFilesFoo.hpp")
        generatedFileFoo = os.path.join(dirs.gen,  "GenHeaderTwoFilesFoo.hpp")

        baseFileBar      = os.path.join(dirs.base, "GenHeaderTwoFilesBar.hpp")
        expectedFileBar  = os.path.join(dirs.ref,  "GenHeaderTwoFilesBar.hpp")
        generatedFileBar = os.path.join(dirs.gen,  "GenHeaderTwoFilesBar.hpp")

        options = headerGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseFileIFoo, baseFileBar]

        checker = headerGen.OptionsConsistencyChecker()
        headerGen.run(options,checker)

        self.cmpFiles(expectedFileFoo, generatedFileFoo)
        self.cmpFiles(expectedFileBar, generatedFileBar)

    def testGenImplementationTwoFiles(self) :
        baseFileFoo      = os.path.join(dirs.base, "GenImplementationTwoFilesFoo.hpp")
        expectedFileFoo  = os.path.join(dirs.ref,  "GenImplementationTwoFilesFoo.cpp")
        generatedFileFoo = os.path.join(dirs.gen,  "GenImplementationTwoFilesFoo.cpp")

        baseFileBar      = os.path.join(dirs.base, "GenImplementationTwoFilesBar.hpp")
        expectedFileBar  = os.path.join(dirs.ref,  "GenImplementationTwoFilesBar.cpp")
        generatedFileBar = os.path.join(dirs.gen,  "GenImplementationTwoFilesBar.cpp")

        options = implementationGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseFileFoo, baseFileBar]

        checker = implementationGen.OptionsConsistencyChecker()
        implementationGen.run(options, checker)

        self.cmpFiles(expectedFileFoo, generatedFileFoo)
        self.cmpFiles(expectedFileBar, generatedFileBar)

    def testGenFactory(self) :
        baseFileIFoo     = os.path.join(dirs.base, "GenFactoryIFoo.hpp")
        expectedFileFooFactoryInterface = os.path.join(dirs.ref, "GenFactoryIFooFactory.hpp")
        expectedFileFooFactoryHeader    = os.path.join(dirs.ref, "GenFactoryFooFactory.hpp")
        expectedFileFooFactoryImpl      = os.path.join(dirs.ref, "GenFactoryFooFactory.cpp")
        generatedFileFooFactoryInterface = os.path.join(dirs.gen, "GenFactoryIFooFactory.hpp")
        generatedFileFooFactoryHeader    = os.path.join(dirs.gen, "GenFactoryFooFactory.hpp")
        generatedFileFooFactoryImpl      = os.path.join(dirs.gen, "GenFactoryFooFactory.cpp")

        options = factoryGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseFileIFoo]

        checker = factoryGen.OptionsConsistencyChecker()
        factoryGen.run(options, checker)

        self.cmpFiles(expectedFileFooFactoryInterface, generatedFileFooFactoryInterface)
        self.cmpFiles(expectedFileFooFactoryHeader, generatedFileFooFactoryHeader)
        self.cmpFiles(expectedFileFooFactoryImpl, generatedFileFooFactoryImpl)

    def testGenMock(self) :
        baseClassToBeMocked = os.path.join(dirs.base, "GenMockFoo.hpp")
        expectedMockFile    = os.path.join(dirs.ref,  "GenMockFooMock.hpp")
        generatedMockFile   = os.path.join(dirs.gen,  "GenMockFooMock.hpp")

        options = mockGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseClassToBeMocked]

        checker = mockGen.OptionsConsistencyChecker()
        mockGen.run(options, checker)

        self.cmpFiles(expectedMockFile, generatedMockFile)

    def testGenMockOperators(self) :
        baseClassToBeMocked = os.path.join(dirs.base, "GenMockOperators.hpp")
        expectedMockFile    = os.path.join(dirs.ref,  "GenMockOperatorsMock.hpp")
        generatedMockFile   = os.path.join(dirs.gen,  "GenMockOperatorsMock.hpp")

        options = mockGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [baseClassToBeMocked]

        checker = mockGen.OptionsConsistencyChecker()
        mockGen.run(options, checker)
        self.cmpFiles(expectedMockFile, generatedMockFile)

    def testGenTestSuite(self) :
        fooClass = os.path.join(dirs.base, "GenTestSuiteFoo.hpp")
        expectedFooClassTestSuite  = os.path.join(dirs.ref,  "GenTestSuiteFooTestSuite.cpp")
        generatedFooClassTestSuite = os.path.join(dirs.gen,  "GenTestSuiteFooTestSuite.cpp")

        options = testsuiteGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [fooClass]

        checker = testsuiteGen.OptionsConsistencyChecker()
        testsuiteGen.run(options, checker)

        self.cmpFiles(expectedFooClassTestSuite, generatedFooClassTestSuite)

    def testGenExtractInterfaces(self) :
        barClass = os.path.join(dirs.base, "ExtractInterfaceBar.hpp")
        expectedBarInterface  = os.path.join(dirs.ref, "ExtractInterfaceIBar.hpp")
        generatedBarInterface = os.path.join(dirs.gen, "ExtractInterfaceIBar.hpp")

        options = testsuiteGen.Options()
        options.force = True
        options.output_dir = dirs.gen
        options.args = [barClass]

        checker = extractInterfacesGen.OptionsConsistencyChecker()
        extractInterfacesGen.run(options, checker)

        self.cmpFiles(expectedBarInterface, generatedBarInterface)

    def testFGenAllWithInterface(self) :
        fGenFilesDir = os.path.join(dirs._base, "fGenFiles")
        fGenGeneratedFilesDir = os.path.join(fGenFilesDir, "Generated")
        fGenReferenceFilesDir = os.path.join(fGenFilesDir, "Reference")

        options = testsuiteGen.Options()
        options.args = ["Aaa::Bbb::IFoo"]
        options.gen_interface = True
        options.gen_extracted_interfaces = False
        options.gen_mock = True
        options.gen_header = True
        options.gen_impl = True
        options.gen_test_suite = True
        options.gen_factory = True
        options.force = True
        options.output_dir = fGenGeneratedFilesDir

        checker = fGen.OptionsConsistencyChecker(options)
        fGen.run(options, checker)

        expectedInterfaceFile = os.path.join(fGenReferenceFilesDir, "AaaBbbIFoo.hpp")
        expectedHeaderFile = os.path.join(fGenReferenceFilesDir,    "AaaBbbFoo.hpp")
        expectedCppFile = os.path.join(fGenReferenceFilesDir,       "AaaBbbFoo.cpp")
        expectedMockFile = os.path.join(fGenReferenceFilesDir,      "AaaBbbFooMock.hpp")
        expectedTestSuiteFile = os.path.join(fGenReferenceFilesDir, "AaaBbbFooTestSuite.cpp")
        expectedFactoryInyerfaceFile = os.path.join(fGenReferenceFilesDir, "AaaBbbIFooFactory.hpp")
        expectedFactoryHeaderFile = os.path.join(fGenReferenceFilesDir,    "AaaBbbFooFactory.hpp")
        expectedFactoryCppFile = os.path.join(fGenReferenceFilesDir,       "AaaBbbFooFactory.cpp")

        generatedInterfaceFile = os.path.join(fGenReferenceFilesDir, "AaaBbbIFoo.hpp")
        generatedHeaderFile = os.path.join(fGenReferenceFilesDir,    "AaaBbbFoo.hpp")
        generatedCppFile = os.path.join(fGenReferenceFilesDir,       "AaaBbbFoo.cpp")
        generatedMockFile = os.path.join(fGenReferenceFilesDir,      "AaaBbbFooMock.hpp")
        generatedTestSuiteFile = os.path.join(fGenReferenceFilesDir, "AaaBbbFooTestSuite.cpp")
        generatedFactoryInyerfaceFile = os.path.join(fGenReferenceFilesDir, "AaaBbbIFooFactory.hpp")
        generatedFactoryHeaderFile = os.path.join(fGenReferenceFilesDir,    "AaaBbbFooFactory.hpp")
        generatedFactoryCppFile = os.path.join(fGenReferenceFilesDir,       "AaaBbbFooFactory.cpp")

        self.cmpFiles(expectedInterfaceFile, generatedInterfaceFile)
        self.cmpFiles(expectedHeaderFile, generatedHeaderFile)
        self.cmpFiles(expectedCppFile, generatedCppFile)
        self.cmpFiles(expectedMockFile, generatedMockFile)
        self.cmpFiles(expectedTestSuiteFile, generatedTestSuiteFile)
        self.cmpFiles(expectedFactoryInyerfaceFile, generatedFactoryInyerfaceFile)
        self.cmpFiles(generatedFactoryHeaderFile, generatedFactoryHeaderFile)
        self.cmpFiles(generatedFactoryCppFile, generatedFactoryCppFile)

    def testFGenAllFromInterface(self) :
        fGenFilesDir = os.path.join(dirs._base, "fGenFiles")
        fGenGeneratedFilesDir = os.path.join(fGenFilesDir, "Generated")
        fGenReferenceFilesDir = os.path.join(fGenFilesDir, "Reference")
        fGenBaseFilesDir = os.path.join(fGenFilesDir, "Base")

        inputInterfaceFile = os.path.join(fGenBaseFilesDir, "QweRtyIBar.hpp")

        options = testsuiteGen.Options()
        options.args = [inputInterfaceFile]
        options.gen_interface = False
        options.gen_extracted_interfaces = False
        options.gen_mock = True
        options.gen_header = True
        options.gen_impl = True
        options.gen_test_suite = True
        options.gen_factory = True
        options.force = True
        options.output_dir = fGenGeneratedFilesDir

        checker = fGen.OptionsConsistencyChecker(options)
        fGen.run(options, checker)

        expectedHeaderFile = os.path.join(fGenReferenceFilesDir,    "QweRtyBar.hpp")
        expectedCppFile = os.path.join(fGenReferenceFilesDir,       "QweRtyBar.cpp")
        expectedMockFile = os.path.join(fGenReferenceFilesDir,      "QweRtyBarMock.hpp")
        expectedTestSuiteFile = os.path.join(fGenReferenceFilesDir, "QweRtyBarTestSuite.cpp")
        expectedFactoryInyerfaceFile = os.path.join(fGenReferenceFilesDir, "QweRtyIBarFactory.hpp")
        expectedFactoryHeaderFile = os.path.join(fGenReferenceFilesDir,    "QweRtyBarFactory.hpp")
        expectedFactoryCppFile = os.path.join(fGenReferenceFilesDir,       "QweRtyBarFactory.cpp")

        generatedHeaderFile = os.path.join(fGenReferenceFilesDir,    "QweRtyBar.hpp")
        generatedCppFile = os.path.join(fGenReferenceFilesDir,       "QweRtyBar.cpp")
        generatedMockFile = os.path.join(fGenReferenceFilesDir,      "QweRtyBarMock.hpp")
        generatedTestSuiteFile = os.path.join(fGenReferenceFilesDir, "QweRtyBarTestSuite.cpp")
        generatedFactoryInyerfaceFile = os.path.join(fGenReferenceFilesDir, "QweRtyIBarFactory.hpp")
        generatedFactoryHeaderFile = os.path.join(fGenReferenceFilesDir,    "QweRtyBarFactory.hpp")
        generatedFactoryCppFile = os.path.join(fGenReferenceFilesDir,       "QweRtyBarFactory.cpp")

        self.cmpFiles(expectedHeaderFile, generatedHeaderFile)
        self.cmpFiles(expectedCppFile, generatedCppFile)
        self.cmpFiles(expectedMockFile, generatedMockFile)
        self.cmpFiles(expectedTestSuiteFile, generatedTestSuiteFile)
        self.cmpFiles(expectedFactoryInyerfaceFile, generatedFactoryInyerfaceFile)
        self.cmpFiles(generatedFactoryHeaderFile, generatedFactoryHeaderFile)
        self.cmpFiles(generatedFactoryCppFile, generatedFactoryCppFile)

if __name__ == "__main__":
    unittest.main()

