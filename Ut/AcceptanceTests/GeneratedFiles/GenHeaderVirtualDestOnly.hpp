/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "GenHeaderIVirtualDestOnly.hpp"

namespace Gen
{
namespace Header
{

class VirtualDestOnly : public IVirtualDestOnly
{
};

} //namespace Header
} //namespace Gen

