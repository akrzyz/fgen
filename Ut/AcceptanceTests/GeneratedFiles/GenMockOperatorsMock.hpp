/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                GenMock
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include "GenMockOperators.hpp"

namespace GenMock
{

class OperatorsMock : public Operators
{
public:
    MOCK_METHOD1(additionAssignmentOperator, Foo &(const Foo));
    virtual Foo & operator+=(const Foo p_foo)
    {
        return additionAssignmentOperator(p_foo);
    }
    MOCK_CONST_METHOD1(lessThanOperator, bool(const Foo &));
    virtual bool operator<(const Foo & p_rhs) const
    {
        return lessThanOperator(p_rhs);
    }
    MOCK_CONST_METHOD1(functionCallOperator, bool(const u16));
    virtual bool operator()(const u16 p_id) const
    {
        return functionCallOperator(p_id);
    }
    MOCK_CONST_METHOD3(functionCallOperator, bool(const u16, int, const Foo &));
    virtual bool operator()(const u16 p_param1, int p_param2, const Foo & p_param3) const
    {
        return functionCallOperator(p_param1, p_param2, p_param3);
    }
    MOCK_CONST_METHOD4(functionCallOperator, bool(const u16, int, const Foo *, const Bar &));
    virtual bool operator()(const u16 p_param1, int p_id, const Foo * p_param2, const Bar & p_bar) const
    {
        return functionCallOperator(p_param1, p_id, p_param2, p_bar);
    }
    MOCK_METHOD1(newOperator, void *(size));
    virtual void * operator new(size T)
    {
        return newOperator(T);
    }
    MOCK_METHOD1(newArrayOperator, void *(size));
    virtual void * operator new[](size T)
    {
        return newArrayOperator(T);
    }
    MOCK_METHOD0(conversionToToTypeOperator, ToType());
    virtual operator ToType()
    {
        return conversionToToTypeOperator();
    }
    MOCK_METHOD0(conversionToConstToTypeOperator, const ToType());
    virtual operator const ToType()
    {
        return conversionToConstToTypeOperator();
    }
    MOCK_METHOD0(conversionToConstToTypeRefOperator, const ToType &());
    virtual operator const ToType &()
    {
        return conversionToConstToTypeRefOperator();
    }
    MOCK_METHOD0(conversionToConstToTypePtrOperator, const ToType *());
    virtual operator const ToType *()
    {
        return conversionToConstToTypePtrOperator();
    }
    MOCK_METHOD0(conversionToToTypePtrOperator, ToType *());
    virtual operator ToType *()
    {
        return conversionToToTypePtrOperator();
    }
    MOCK_METHOD0(conversionToIntOperator, int());
    virtual operator int()
    {
        return conversionToIntOperator();
    }
};

} //namespace GenMock

