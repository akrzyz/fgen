/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "GenHeaderTwoFilesIFoo.hpp"

namespace Gen
{
namespace Header
{
namespace TwoFiles
{

class Foo : public IFoo
{
public:
    void abstractFoo() override;
};

} //namespace TwoFiles
} //namespace Header
} //namespace Gen

