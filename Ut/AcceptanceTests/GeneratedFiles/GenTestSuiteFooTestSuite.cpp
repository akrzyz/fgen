/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                GenTestSuite
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include <gtest/gtest.h>
#include "GenTestSuiteFoo.hpp"

namespace GenTestSuite
{

using namespace ::testing;

class FooTestSuite : public Test
{
};

TEST_F(FooTestSuite, TestDummy)
{
    EXPECT_TRUE(false);
}

} //namespace GenTestSuite

