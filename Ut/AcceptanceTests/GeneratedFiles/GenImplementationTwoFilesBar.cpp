/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                GenImplementationTwoFiles
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include "GenImplementationTwoFilesBar.hpp"

namespace GenImplementationTwoFiles
{

int Bar::getId()
{
}

const std::string Bar::getNameProtected(Aaa::Id id)
{
}

void Bar::doPrivateStuffs()
{
}

} //namespace GenImplementationTwoFiles

