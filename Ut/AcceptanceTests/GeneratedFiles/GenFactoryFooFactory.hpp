/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "GenFactoryIFooFactory.hpp"

namespace Gen
{
namespace Factory
{

class FooFactory : public IFooFactory
{
public:
    std::shared_ptr<IFoo> create() override;
};

} //namespace Factory
} //namespace Gen

