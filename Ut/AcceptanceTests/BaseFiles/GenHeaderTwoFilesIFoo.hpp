/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Gen
{
namespace Header
{
namespace TwoFiles
{

struct IFoo
{
    virtual ~IFoo() {};
    virtual void abstractFoo() = 0;
    virtual void virtualFoo();
    virtual void regularFoo();
};

} //namespace TwoFile
} //namespace Header
} //namespace Gen

