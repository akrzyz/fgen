/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Extract
{
namespace Interface
{

struct Bar
{
    void ~Bar();
    void Bar();
    void publicMethodFoo(int);
    const std::string & publicMethodFoo2(const int & p_myInt);
protected:
    void protectedMethodFoo(int);
private:
    void privateMethodFoo(int);
};

} //namespace Extract
} //namespace Interface

