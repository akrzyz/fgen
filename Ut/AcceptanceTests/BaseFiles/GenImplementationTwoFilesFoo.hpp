/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Gen
{
namespace Implementation
{
namespace TwoFiles
{

struct Foo
{
    Foo(int id, int count);
    virtual ~Foo();
    virtual void abstractFoo() = 0;
    virtual void virtualFoo(int id, int count);
    void overridedFoo(int) override;
    void regularFoo(double);
};

} //namespace TwoFile
} //namespace Implementation
} //namespace Gen

