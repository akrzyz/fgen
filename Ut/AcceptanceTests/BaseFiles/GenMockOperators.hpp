/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace GenMock
{

struct Operators
{
    virtual Foo & operator += (const Foo p_foo);
    virtual bool operator<(const Foo & p_rhs) const;
    virtual bool operator()(const u16 p_id) const;
    virtual bool operator()(const u16, int, const Foo &) const;
    virtual bool operator()(const u16, int p_id, const Foo *, const Bar & p_bar) const;
    virtual void * operator new(size T);
    virtual void * operator new[](size T);
    virtual operator ToType();
    virtual operator const ToType();
    virtual operator const ToType & ();
    virtual operator const ToType * ();
    virtual operator ToType * ();
    virtual operator int ();
};

} //namespace GenMock

