/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Gen
{
namespace Factory
{

struct IFoo
{
    virtual ~Foo() {};
    virtual void abstractFoo(double) = 0;
};

} //namespace Factory
} //namespace Gen

