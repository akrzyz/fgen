/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace GenMock
{

struct Foo
{
    virtual void foo(int);
    virtual void foo(int *);
    virtual void fooNamedParam(int p_id);
    virtual void abstractFoo() = 0;
    virtual const std::string & getName();
    virtual bool bar(const std::string & , int ) const;
    virtual bool bar(const std::string & p_name) const;
    virtual bool bar(const char * , int p_id ) const;
    virtual bool stop();
    void notVirtualMethod();
    void overridedMethod() override;

    int publicMember;
private:
    void dumyPrivat();
    virtual void virtualDumyPrivat();
    virtual void abstractDumyPrivat() = 0;

    int privateMember;
};

} //namespace GenMock

