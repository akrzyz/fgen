/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace GenHeaderTwoFiles
{

struct Bar
{
    Bar(int p_param);
    virtual void setId(p_id) = 0;
    virtual int getId() = 0;
};

} //namespace GenHeaderTwoFiles

