/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace GenImplementationTwoFiles
{

struct Bar
{
    Bar(int param) :
        param(param)
    {
    }
    virtual ~Bar() {}
    void inlineBar(id) {this.id = id};
    virtual int getId();

protected :
    const std::string getNameProtected(Aaa::Id id);

private :
    void doPrivateStuffs();

    int id;
    int param;

};

} //namespace GenImplementationTwoFiles

