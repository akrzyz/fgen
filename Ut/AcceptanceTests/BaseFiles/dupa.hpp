/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Gen
{
namespace Header
{
namespace TwoFiles
{

struct IFoo
{
    void overridedFoo() override;
    virtual void virtualOverridedFoo() override;
    void constOverridedFoo() const override;
};

} //namespace TwoFile
} //namespace Header
} //namespace Gen

