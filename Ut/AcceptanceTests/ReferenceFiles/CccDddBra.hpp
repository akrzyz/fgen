/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Ccc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

namespace Ccc
{
namespace Ddd
{

class Bra
{
public:
    virtual ~Bra() {}
};

} //namespace Ddd
} //namespace Ccc

