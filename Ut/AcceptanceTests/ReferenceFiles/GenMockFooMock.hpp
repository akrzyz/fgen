/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                GenMock
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include "GenMockFoo.hpp"

namespace GenMock
{

class FooMock : public Foo
{
public:
    MOCK_METHOD1(foo, void(int));
    MOCK_METHOD1(foo, void(int *));
    MOCK_METHOD1(fooNamedParam, void(int));
    MOCK_METHOD0(abstractFoo, void());
    MOCK_METHOD0(getName, const std::string &());
    MOCK_CONST_METHOD2(bar, bool(const std::string &, int));
    MOCK_CONST_METHOD1(bar, bool(const std::string &));
    MOCK_CONST_METHOD2(bar, bool(const char *, int));
    MOCK_METHOD0(stop, bool());
    MOCK_METHOD0(overridedMethod, void());
};

} //namespace GenMock

