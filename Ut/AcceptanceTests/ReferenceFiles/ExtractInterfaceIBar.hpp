/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Extract
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

namespace Extract
{
namespace Interface
{

class IBar
{
public:
    virtual ~IBar() {}
    virtual void publicMethodFoo(int) = 0;
    virtual const std::string & publicMethodFoo2(const int &) = 0;
};

} //namespace Interface
} //namespace Extract

