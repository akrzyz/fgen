/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include "GenImplementationTwoFilesFoo.hpp"

namespace Gen
{
namespace Implementation
{
namespace TwoFiles
{

Foo::Foo(int id, int count)
{
}

Foo::~Foo()
{
}

void Foo::virtualFoo(int id, int count)
{
}

void Foo::overridedFoo(int p_param1)
{
}

void Foo::regularFoo(double p_param1)
{
}

} //namespace TwoFiles
} //namespace Implementation
} //namespace Gen

