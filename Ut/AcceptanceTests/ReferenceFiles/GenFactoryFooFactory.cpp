/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include "GenFactoryFooFactory.hpp"

namespace Gen
{
namespace Factory
{

std::shared_ptr<IFoo> FooFactory::create()
{
}

} //namespace Factory
} //namespace Gen

