/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

namespace Aaa
{
namespace Bbb
{

class IFoo
{
public:
    virtual ~IFoo() {}
};

} //namespace Bbb
} //namespace Aaa

