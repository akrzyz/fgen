/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Gen
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include <memory>
#include "GenFactoryIFoo.hpp"

namespace Gen
{
namespace Factory
{

class IFooFactory
{
public:
    virtual ~IFooFactory() {}
    virtual std::shared_ptr<IFoo> create() = 0;
};

} //namespace Factory
} //namespace Gen

