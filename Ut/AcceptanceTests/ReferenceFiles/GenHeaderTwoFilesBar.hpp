/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                GenHeaderTwoFiles
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "GenHeaderTwoFilesBar.hpp"

namespace GenHeaderTwoFiles
{

class Bar : public Bar
{
public:
    void setId(p_id) override;
    int getId() override;
};

} //namespace GenHeaderTwoFiles

