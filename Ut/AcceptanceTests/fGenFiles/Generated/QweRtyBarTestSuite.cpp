/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include <gtest/gtest.h>
#include "QweRtyBar.hpp"

namespace Qwe
{
namespace Rty
{

using namespace ::testing;

class BarTestSuite : public Test
{
};

TEST_F(BarTestSuite, TestDummy)
{
    EXPECT_TRUE(false);
}

} //namespace Rty
} //namespace Qwe

