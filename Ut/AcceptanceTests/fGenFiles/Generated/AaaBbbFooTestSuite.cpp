/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include <gtest/gtest.h>
#include "AaaBbbFoo.hpp"

namespace Aaa
{
namespace Bbb
{

using namespace ::testing;

class FooTestSuite : public Test
{
};

TEST_F(FooTestSuite, TestDummy)
{
    EXPECT_TRUE(false);
}

} //namespace Bbb
} //namespace Aaa

