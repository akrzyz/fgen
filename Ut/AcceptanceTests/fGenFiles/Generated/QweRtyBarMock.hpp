/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include "QweRtyIBar.hpp"

namespace Qwe
{
namespace Rty
{

class BarMock : public IBar
{
public:
    MOCK_METHOD0(name, const std::string &());
    MOCK_METHOD0(start, void());
    MOCK_METHOD1(stop, bool(int));
};

} //namespace Rty
} //namespace Qwe

