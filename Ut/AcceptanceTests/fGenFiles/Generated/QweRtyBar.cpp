/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#include "QweRtyBar.hpp"

namespace Qwe
{
namespace Rty
{

const std::string & Bar::name()
{
}

void Bar::start()
{
}

bool Bar::stop(int p_param1)
{
}

} //namespace Rty
} //namespace Qwe

