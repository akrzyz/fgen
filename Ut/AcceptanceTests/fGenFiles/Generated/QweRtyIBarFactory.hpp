/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include <memory>
#include "QweRtyIBar.hpp"

namespace Qwe
{
namespace Rty
{

class IBarFactory
{
public:
    virtual ~IBarFactory() {}
    virtual std::shared_ptr<IBar> create() = 0;
};

} //namespace Rty
} //namespace Qwe

