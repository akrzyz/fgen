/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "QweRtyIBar.hpp"

namespace Qwe
{
namespace Rty
{

class Bar : public IBar
{
public:
    const std::string & name() override;
    void start() override;
    bool stop(int) override;
};

} //namespace Rty
} //namespace Qwe

