/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "AaaBbbIFooFactory.hpp"

namespace Aaa
{
namespace Bbb
{

class FooFactory : public IFooFactory
{
public:
    std::shared_ptr<IFoo> create() override;
};

} //namespace Bbb
} //namespace Aaa

