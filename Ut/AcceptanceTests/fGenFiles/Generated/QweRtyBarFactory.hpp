/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia. All rights reserved.
**********************************************************************************/

#pragma once

#include "QweRtyIBarFactory.hpp"

namespace Qwe
{
namespace Rty
{

class BarFactory : public IBarFactory
{
public:
    std::shared_ptr<IBar> create() override;
};

} //namespace Rty
} //namespace Qwe

