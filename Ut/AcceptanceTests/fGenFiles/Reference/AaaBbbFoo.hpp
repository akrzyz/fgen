/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <AaaBbbIFoo.hpp>

namespace Aaa
{
namespace Bbb
{

struct Foo : public IFoo
{
};

} //namespace Bbb
} //namespace Aaa

