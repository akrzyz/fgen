/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#include <gtest/gtest.h>
#include <QweRtyBar.hpp>

namespace Qwe
{
namespace Rty
{
namespace Ut
{

using namespace ::testing;

struct BarTestSuite : public Test
{
};

TEST_F(BarTestSuite, TestDummy)
{
    EXPECT_TRUE(false);
}

} //namespace Ut
} //namespace Rty
} //namespace Qwe

