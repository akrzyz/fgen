/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <QweRtyIBar.hpp>

namespace Qwe
{
namespace Rty
{

struct Bar : public IBar
{
    virtual const std::string & name() override;
    virtual void start() override;
    virtual bool stop(int) override;
};

} //namespace Rty
} //namespace Qwe

