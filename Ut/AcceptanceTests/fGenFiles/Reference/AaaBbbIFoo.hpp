/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Aaa
{
namespace Bbb
{

struct IFoo
{
    virtual ~IFoo() {}
};

} //namespace Bbb
} //namespace Aaa

