/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#include <gtest/gtest.h>
#include <AaaBbbFoo.hpp>

namespace Aaa
{
namespace Bbb
{
namespace Ut
{

using namespace ::testing;

struct FooTestSuite : public Test
{
};

TEST_F(FooTestSuite, TestDummy)
{
    EXPECT_TRUE(false);
}

} //namespace Ut
} //namespace Bbb
} //namespace Aaa

