/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <AaaBbbIFoo.hpp>

namespace Aaa
{
namespace Bbb
{
namespace Ut
{

struct FooMock : public IFoo
{
};

} //namespace Ut
} //namespace Bbb
} //namespace Aaa

