/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Qwe
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <memory>
#include <QweRtyIBar.hpp>

namespace Qwe
{
namespace Rty
{

struct IBarFactory
{
    virtual ~IBarFactory() {}
    virtual std::shared_ptr<IBar> create() = 0;
};

} //namespace Rty
} //namespace Qwe

