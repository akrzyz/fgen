/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <AaaBbbIFooFactory.hpp>

namespace Aaa
{
namespace Bbb
{

struct FooFactory : public IFooFactory
{
    virtual std::shared_ptr<IFoo> create() override;
};

} //namespace Bbb
} //namespace Aaa

