/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

#include <memory>
#include <AaaBbbIFoo.hpp>

namespace Aaa
{
namespace Bbb
{

struct IFooFactory
{
    virtual ~IFooFactory() {}
    virtual std::shared_ptr<IFoo> create() = 0;
};

} //namespace Bbb
} //namespace Aaa

