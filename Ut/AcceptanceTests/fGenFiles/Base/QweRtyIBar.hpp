/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Aaa
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Solutions and Networks. All rights reserved.
**********************************************************************************/

#pragma once

namespace Qwe
{
namespace Rty
{

struct IBar
{
    virtual ~IBar() {}
    virtual const std::string & name() = 0;
    virtual void start() = 0;
    virtual bool stop(int) = 0;
};

} //namespace Bbb
} //namespace Aaa

