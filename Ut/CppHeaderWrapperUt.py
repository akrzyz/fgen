#!/usr/bin/env python3
import unittest
from  gen.CppHeaderParserWrappers import *
from gen.consts import *
import copy

class functionsImplFromClassTestCase(unittest.TestCase):

    def setUp(self):

        self.paramWithName = {NAME : "p_id", TYPE : "int"}
        self.paramWithoutName = {NAME : "", TYPE : "int"}

        self.methodFoo = {
            NAME         : "foo",
            RETURN_TYPE  : "void",
            PARAMETERS   : [self.paramWithoutName],
            CONST        : False,
            STATIC       : False,
            PURE_VIRTUAL : False,
            VIRTUAL      : False,
            CTOR         : False,
            DTOR         : False,
            DEBUG        : "void foo( );"
        }

        self.methodBarOperator = {
            NAME         : "operator Bar",
            RETURN_TYPE  : "Bar",
            PARAMETERS   : [self.paramWithName],
            CONST        : True,
            STATIC       : True,
            PURE_VIRTUAL : True,
            VIRTUAL      : True,
            CTOR         : True,
            DTOR         : True,
            DEBUG        : "virtual operator Bar ( )"
        }

        self.classFoo = {
            NAME      : "Foo",
            NAMESPACE : "Aaa::Bbb::",
            METHODS   : {
                ACCESS_PUBLIC : [self.methodFoo],
                ACCESS_PRIVARE : [],
                ACCESS_PROTECTED : [self.methodBarOperator]
            }
        }

        self.classBar = {
            NAME      : "Bar",
            NAMESPACE : "Aaa::Bbb::",
            METHODS   : {
                ACCESS_PUBLIC : [],
                ACCESS_PRIVARE : [self.methodFoo],
                ACCESS_PROTECTED : []
            }
        }

        class Header :
            self.classes = {}
            self.functions = []
        self.header = Header()
        self.header.classes = {
                "Foo" : self.classFoo,
                "Bar" : self.classBar
            }
        self.header.functions = [self.methodFoo, self.methodBarOperator]

    def cmpParams(self, rawParamDataList, cppParamsList) :
        for cppParam, rawParam in zip(cppParamsList, rawParamDataList) :
            self.assertEqual(cppParam, CppParameter(rawParam))

    def cmpMethods(self, rawClassData, cppClass) :
        self.compareFunctionsList(rawClassData[METHODS][ACCESS_PUBLIC], cppClass.publicMethods())
        self.compareFunctionsList(rawClassData[METHODS][ACCESS_PROTECTED], cppClass.protectedMethods())
        self.compareFunctionsList(rawClassData[METHODS][ACCESS_PRIVARE], cppClass.privateMethods())
        for cppMethods, rawMethods in zip(cppClass.methods(), (rawClassData[METHODS][ACCESS_PUBLIC],
                                                               rawClassData[METHODS][ACCESS_PROTECTED],
                                                               rawClassData[METHODS][ACCESS_PRIVARE])) :
            self.compareFunctionsList(rawMethods, cppMethods)

    def compareFunctionsList(self, rawFunctionList, cppFunctionList) :
        self.assertEqual(len(rawFunctionList), len(cppFunctionList))
        for rawMethod, cppMethod in zip(rawFunctionList, cppFunctionList) :
            self.assertEqual(CppFunction(rawMethod), cppMethod)

    def cmpClasses(self, rawClassDataList, cppClassList) :
        for rawClassData, cppClass in zip(rawClassDataList, cppClassList) :
            self.assertEqual(CppClass(rawClassData), cppClass)

    def testCppParameterWithOutName(self) :
        param = CppParameter(self.paramWithoutName)
        self.assertEqual(self.paramWithoutName[NAME], param.name())
        self.assertEqual(self.paramWithoutName[TYPE], param.type())

    def testCppParameterWithName(self) :
        param = CppParameter(self.paramWithName)
        self.assertEqual(self.paramWithName[NAME], param.name())
        self.assertEqual(self.paramWithName[TYPE], param.type())

    def testCppParameterEqOperator(self) :
        self.assertEqual(CppParameter(self.paramWithoutName), CppParameter(self.paramWithoutName))
        self.assertEqual(CppParameter(self.paramWithName), CppParameter(self.paramWithName))
        self.assertNotEqual(CppParameter(self.paramWithoutName), CppParameter(self.paramWithName))
        self.assertRaises(TypeError, CppParameter(self.paramWithoutName).__eq__, 1)

    def testCppFunctionFoo(self) :
        foo = CppFunction(self.methodFoo)
        self.assertEqual("foo" , foo.name())
        self.assertEqual("void", foo.returnType())
        self.assertEqual(False, foo.isVirtual())
        self.assertEqual(False, foo.isPureVirtual())
        self.assertEqual(False, foo.isConst())
        self.assertEqual(False, foo.isStatic())
        self.assertEqual(False, foo.isConstructor())
        self.assertEqual(False, foo.isDestructor())
        self.assertEqual(False, foo.isOperator())
        self.assertEqual(True,  foo.isRegularMethod())
        self.assertEqual(True,  foo.isDeclaration())
        self.assertEqual(False, foo.isDefinition())
        self.cmpParams(self.methodFoo[PARAMETERS], foo.parameters())

    def testCppFunctionBarOperator(self) :
        bar = CppFunction(self.methodBarOperator)
        self.assertEqual("operator Bar", bar.name())
        self.assertEqual("Bar", bar.returnType())
        self.assertEqual(True,  bar.isVirtual())
        self.assertEqual(True,  bar.isPureVirtual())
        self.assertEqual(True,  bar.isConst())
        self.assertEqual(True,  bar.isStatic())
        self.assertEqual(True,  bar.isConstructor())
        self.assertEqual(True,  bar.isDestructor())
        self.assertEqual(True,  bar.isOperator())
        self.assertEqual(False, bar.isRegularMethod())
        self.assertEqual(False, bar.isDeclaration())
        self.assertEqual(True,  bar.isDefinition())
        self.cmpParams(self.methodBarOperator[PARAMETERS], bar.parameters())

    def testCppFunctionEqOperator(self) :
        self.assertEqual(CppFunction(self.methodBarOperator), CppFunction(self.methodBarOperator))
        self.assertEqual(CppFunction(self.methodFoo), CppFunction(self.methodFoo))
        self.assertNotEqual(CppFunction(self.methodBarOperator), CppFunction(self.methodFoo))
        self.assertRaises(TypeError, CppFunction(self.methodFoo).__eq__, 1)

    def testCppClassFoo(self) :
        classFoo = CppClass(self.classFoo)
        self.assertEqual("Foo", classFoo.name())
        self.assertEqual("Aaa::Bbb::", classFoo.namespace())
        self.cmpMethods(self.classFoo, classFoo)

    def testCppClassBar(self) :
        classBar = CppClass(self.classBar)
        self.assertEqual("Bar", classBar.name())
        self.assertEqual("Aaa::Bbb::", classBar.namespace())
        self.cmpMethods(self.classBar, classBar)

    def testCppClassEqOperator(self) :
        self.assertEqual(CppClass(self.classFoo), CppClass(self.classFoo))
        self.assertEqual(CppClass(self.classBar), CppClass(self.classBar))
        self.assertNotEqual(CppClass(self.classFoo), CppClass(self.classBar))
        self.assertRaises(TypeError, CppClass(self.classFoo).__eq__, 1)

    def testCppHeader(self) :
        header = CppHeader(self.header)
        self.cmpClasses(self.header.classes.values(), header.classes())
        self.compareFunctionsList(self.header.functions, header.freeFunctions())

if __name__ == "__main__":
    unittest.main()

