#!/usr/bin/env python3
import unittest
from  gen.commons import *
import collections

class CommonsTestCase(unittest.TestCase):

    def testWrapNamespace(self):
        namespace = "Enbc::InterFreq::Ut"
        toBeWrapped = "DEADBEEF"

        s = "namespace Enbc\n"
        s+= "{\n"
        s+= "namespace InterFreq\n"
        s+= "{\n"
        s+= "namespace Ut\n"
        s+= "{\n"
        s+= "\n"
        s+= "DEADBEEF"
        s+= "\n"
        s+= "} //namespace Ut\n"
        s+= "} //namespace InterFreq\n"
        s+= "} //namespace Enbc\n"

        self.assertEqual(s, wrapNamespaces(namespace, toBeWrapped))

    def testWrapNamespace2(self):
        namespace = "Enbc::InterFreq::"
        toBeWrapped = "DEADBEEF"

        s = "namespace Enbc\n"
        s+= "{\n"
        s+= "namespace InterFreq\n"
        s+= "{\n"
        s+= "\n"
        s+= "DEADBEEF"
        s+= "\n"
        s+= "} //namespace InterFreq\n"
        s+= "} //namespace Enbc\n"

        self.assertEqual(s, wrapNamespaces(namespace, toBeWrapped))

    def testWrapNamespaceEmpty(self):
        namespace = ""
        toBeWrapped = "DEADBEEF"
        self.assertEqual(toBeWrapped, wrapNamespaces(namespace, toBeWrapped))

    def testGetInclude(self):
        file = "MyClass.hpp"
        self.assertEqual("#include <MyClass.hpp>\n", globalInclude(file))

    def testGetLocalInclude(self):
        file = "MyClass.hpp"
        self.assertEqual("#include \"MyClass.hpp\"\n", localInclude(file))

    def testGetHeaderGuard(self):
        self.assertEqual("#pragma once\n", headerGuard())

    def testGetUsingNamespace(self):
        namespace = "::Bla::TraTata"
        self.assertEqual("using namespace "+namespace+";\n", usingNamespace(namespace))

    def testCreateSourceFile_noIncludes_noNamespaces(self):
        content = "DEADBEEF\n"
        s = fileHeader("")
        s+= "\n"
        s+= content
        s+= "\n"
        self.assertEqual(s, createSourceFile(content))

    def testCreateSourceFile_noIncludes(self):
        content = "DEADBEEF\n"
        namespace = "UEC::LoadBal"
        s = fileHeader("UEC")
        s+= "\n"
        s+= wrapNamespaces(namespace, content)
        s+= "\n"
        self.assertEqual(s, createSourceFile(content, namespace))

    def testCreateSourceFile(self):
        content = "DEADBEEF\n"
        namespace = "UEC::LoadBal"
        s = fileHeader("UEC")
        s+= "\n"
        s+= globalInclude("bar.hpp")
        s+= localInclude("foo.hpp")
        s+= "\n"
        s+= wrapNamespaces(namespace, content)
        s+= "\n"
        self.assertEqual(s, createSourceFile(content, namespace, ["foo.hpp"], ["bar.hpp"]))

    def testCreateHeaderFile_noIncludes_noNamespaces(self):
        content = "DEADBEEF\n"
        s = fileHeader("")
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= content
        s+= "\n"
        self.assertEqual(s, createHeaderFile(content))

    def testCreateHeaderFile_noIncludes(self):
        content = "DEADBEEF\n"
        namespace = "UEC::LoadBal"
        s = fileHeader("UEC")
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= wrapNamespaces(namespace, content)
        s+= "\n"
        self.assertEqual(s, createHeaderFile(content, namespace))

    def testCreateHeaderFile(self):
        content = "DEADBEEF\n"
        namespace = "UEC::LoadBal"
        s = fileHeader("UEC")
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= globalInclude("bar.hpp")
        s+= localInclude("foo.hpp")
        s+= "\n"
        s+= wrapNamespaces(namespace, content)
        s+= "\n"
        self.assertEqual(s, createHeaderFile(content, namespace,["foo.hpp"],["bar.hpp"]))

    def testJoinNamespaces(self):
        self.assertEqual("Bar::Boo", joinNamespaces("Bar::Boo",""))
        self.assertEqual("::Bar::Boo", joinNamespaces("::Bar::Boo",""))
        self.assertEqual("Bar::Boo::Foo", joinNamespaces("Bar::Boo","Foo"))
        self.assertEqual("Bar::Boo::Foo", joinNamespaces("Bar::Boo","::Foo"))
        self.assertEqual("Bar::Boo::Foo::Folk", joinNamespaces("Bar::Boo","Foo::Folk"))
        self.assertEqual("Bar::Boo::Foo::Folk", joinNamespaces("Bar::Boo","::Foo::Folk"))
        self.assertEqual("::Bar::Boo::Foo", joinNamespaces("::Bar::Boo","Foo"))
        self.assertEqual("Bar::Boo", joinNamespaces("Bar::Boo","::"))
        self.assertEqual("Bar::Boo::Foo", joinNamespaces("Bar::Boo","Foo::"))

    def testCreateFileName(self):
        self.assertEqual("BarBooFoo.hpp", createFileName("Bar::Boo","Foo",HPP))
        self.assertEqual("BarBooFoo.hpp", createFileName("::Bar::Boo","Foo",HPP))
        self.assertEqual("BarBooFoo.hpp", createFileName("Bar::Boo::","Foo",HPP))
        self.assertEqual("BarBooFoo.hpp", createFileName("::Bar::Boo::","Foo",HPP))
        self.assertEqual("BarBoo.hpp", createFileName("Bar::Boo","",HPP))

    def testClassEmpty(self):
        className = "Foo"
        s  = "class %s\n" % className
        s += "{\n"
        s += "};\n"
        self.assertEqual(s, str(Class(className)))

    def testClassWithContent(self):
        className = "Foo"
        classContent =  "    void foo();\n"
        classContent += "    void bar(int);\n"
        s  = "class %s\n" % className
        s += "{\n"
        s += classContent
        s += "};\n"
        self.assertEqual(s, str(Class(className, classContent)))

    def testClassWithSinglePublicInheritance(self):
        className = "Foo"
        publicInterface = "IFoo"
        inheritancesMap = {ACCESS_PUBLIC : [publicInterface]}
        s  = "class %s : public %s\n" % (className, publicInterface)
        s += "{\n"
        s += "};\n"
        self.assertEqual(s, str(Class(className, "", inheritancesMap)))

    def testClassWithTwoPublicInheritance(self):
        className = "Foo"
        publicInterface1 = "IFoo"
        publicInterface2 = "IBar"
        inheritancesMap = {ACCESS_PUBLIC : [publicInterface1, publicInterface2]}
        s  = "class %s : public %s, public %s\n" % (className, publicInterface1, publicInterface2)
        s += "{\n"
        s += "};\n"
        self.assertEqual(s, str(Class(className, "", inheritancesMap)))

    def testClassWithMixedInheritance(self):
        className = "Foo"
        publicInterface = "IFoo"
        protectedInterface = "IBar"
        privateInterface = "IRif"
        inheritancesMap = collections.OrderedDict()
        inheritancesMap[ACCESS_PUBLIC] = [publicInterface]
        inheritancesMap[ACCESS_PROTECTED] = [protectedInterface]
        inheritancesMap[ACCESS_PRIVARE] = [privateInterface]
        s  = "class %s : public %s, protected %s, private %s\n" % (className, publicInterface, protectedInterface, privateInterface)
        s += "{\n"
        s += "};\n"
        self.assertEqual(s, str(Class(className, "", inheritancesMap)))

    def testClassDestructor(self) :
        className = "Foo"
        s = "virtual ~%s() {}\n" % className
        self.assertEqual(s, virtualDestructor(className))

if __name__ == "__main__":
    unittest.main()

