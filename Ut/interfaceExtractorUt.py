#!/usr/bin/env python3
import unittest
from mock import MagicMock
from mock import Mock
import copy
from  gen.interfaceExtractor import *
from  gen.CppHeaderParserWrappers import *
from CppWrappersMocks import *

class ExtractInterfaceTestCase(unittest.TestCase):
    def setUp(self) :

        functionBaseMock = copy.deepcopy(functionDefaultMock)
        functionBaseMock.isVirtual.return_value = True
        functionBaseMock.isPureVirtual.return_value = True

        self.publicFunctionMock = copy.deepcopy(functionBaseMock)
        self.publicFunctionMock.name.return_value = "publicFoo"

        self.protectedFunctionMock = copy.deepcopy(functionBaseMock)
        self.protectedFunctionMock.name.return_value = "protectedFoo"

        self.privateFunctionMock = copy.deepcopy(functionBaseMock)
        self.privateFunctionMock.name.return_value = "privateFoo"

        self.notAbstractFunctionMock = copy.deepcopy(functionBaseMock)
        self.notAbstractFunctionMock.name.return_value = "notAbstractFoo"
        self.notAbstractFunctionMock.isPureVirtual.return_value = False

        self.classMock = copy.deepcopy(classStrictMock)
        self.classMock.name = Mock(return_value = "Bar")
        self.classMock.namespace = Mock(return_value = "Aaa::Bbb::")
        self.classMock.publicMethods    = Mock(return_value = [self.publicFunctionMock, self.notAbstractFunctionMock])
        self.classMock.protectedMethods = Mock(return_value = [self.protectedFunctionMock])
        self.classMock.privateMethods   = Mock(return_value = [self.privateFunctionMock])

    def testExtractedInterfaceClass(self) :
        s = "class IBar\n"
        s+= "{\n"
        s+= "public:\n"
        s+= "    virtual ~IBar() {}\n"
        s+= "    virtual void publicFoo() = 0;\n"
        s+= "    virtual void notAbstractFoo() = 0;\n"
        s+= "};\n"

        self.assertEqual(s, str(ExtractedInterfaceClass(self.classMock, "Bar")))

    def testExtractedInterfaceFile(self) :
        ifFile = "AaaBbbIBar.hpp"
        fileName = "AaaBbbBar.hpp"

        body = wrapNamespaces(self.classMock.namespace(), str(ExtractedInterfaceClass(self.classMock, "Bar")))

        s = fileHeader("Aaa")
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= body
        s+= "\n"

        interfaceFile = ExtractedInterfaceFile(self.classMock)
        self.assertEqual(ifFile, interfaceFile.name)
        self.assertEqual(s, str(interfaceFile))

if __name__ == "__main__":
    unittest.main()

