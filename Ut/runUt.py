#!/usr/bin/env python3
import os.path
import unittest

def suite():
    return unittest.TestLoader().discover(".","*.py")

if __name__ == '__main__' :
    unittest.main(defaultTest="suite", verbosity=2)
