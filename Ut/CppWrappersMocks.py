#!/usr/bin/env python3
from mock import MagicMock
from mock import Mock
import copy

class UninterestingMockFunctionCall(Exception) :
    pass

functionStrictMock = Mock()
functionStrictMock.name.side_effect = UninterestingMockFunctionCall
functionStrictMock.returnType.side_effect = UninterestingMockFunctionCall
functionStrictMock.parameters.side_effect = UninterestingMockFunctionCall
functionStrictMock.isVirtual.side_effect = UninterestingMockFunctionCall
functionStrictMock.isPureVirtual.side_effect = UninterestingMockFunctionCall
functionStrictMock.isConst.side_effect = UninterestingMockFunctionCall
functionStrictMock.isStatic.side_effect = UninterestingMockFunctionCall
functionStrictMock.isConstructor.side_effect = UninterestingMockFunctionCall
functionStrictMock.isDestructor.side_effect = UninterestingMockFunctionCall
functionStrictMock.isOperator.side_effect = UninterestingMockFunctionCall
functionStrictMock.isRegularMethod.side_effect = UninterestingMockFunctionCall
functionStrictMock.isDeclaration.side_effect = UninterestingMockFunctionCall
functionStrictMock.isDefinition.side_effect = UninterestingMockFunctionCall

functionDefaultMock = Mock()
functionDefaultMock.name.return_value = "foo"
functionDefaultMock.returnType.return_value = "void"
functionDefaultMock.parameters.return_value = []
functionDefaultMock.isVirtual.return_value = False
functionDefaultMock.isPureVirtual.return_value = False
functionDefaultMock.isConst.return_value = False
functionDefaultMock.isStatic.return_value = False
functionDefaultMock.isConstructor.return_value = False
functionDefaultMock.isDestructor.return_value = False
functionDefaultMock.isOperator.return_value = False
functionDefaultMock.isRegularMethod.return_value = True
functionDefaultMock.isDeclaration.return_value = True
functionDefaultMock.isDefinition.return_value = False

operatorDefaultMock = copy.deepcopy(functionDefaultMock)
operatorDefaultMock.name.return_value = "operator"
operatorDefaultMock.isOperator.return_value = True
operatorDefaultMock.isRegularMethod.return_value = False
operatorDefaultMock.isVirtual.return_value = True

constructorDefaultMock = copy.deepcopy(functionDefaultMock)
constructorDefaultMock.name.return_value = "Bar"
constructorDefaultMock.isRegularMethod.return_value = False
constructorDefaultMock.isConstructor.return_value = True

destructorDefaultMock = copy.deepcopy(functionDefaultMock)
destructorDefaultMock.name.return_value = "Bar"
destructorDefaultMock.isDestructor.return_value = True
destructorDefaultMock.isRegularMethod.return_value = False

classStrictMock = Mock()
classStrictMock.name.side_effect = UninterestingMockFunctionCall
classStrictMock.namespace.side_effect = UninterestingMockFunctionCall
classStrictMock.publicMethods.side_effect = UninterestingMockFunctionCall
classStrictMock.protectedMethods.side_effect = UninterestingMockFunctionCall
classStrictMock.privateMethods.side_effect = UninterestingMockFunctionCall
classStrictMock.methods.side_effect = UninterestingMockFunctionCall

classDefaultMock = Mock()
classDefaultMock.name.return_value = "Bar"
classDefaultMock.namespace.return_value = ""
classDefaultMock.publicMethods.return_value = []
classDefaultMock.protectedMethods.return_value = []
classDefaultMock.privateMethods.return_value = []
classDefaultMock.methods.return_value = [[]]

parameterStrictMock = Mock()
parameterStrictMock.type.side_effect = UninterestingMockFunctionCall
parameterStrictMock.name.side_effect = UninterestingMockFunctionCall

parameterDefaultMock = Mock()
parameterDefaultMock.type.return_value = "int"
parameterDefaultMock.name.return_value = ""

