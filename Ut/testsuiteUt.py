#!/usr/bin/env python3
import copy
import unittest
from gen.testsuite import *
from gen.CppHeaderParserWrappers import *
from mock import patch,Mock
from CppWrappersMocks import *

class TestSuiteTestCase(unittest.TestCase):

    def setUp(self):

        self.classMock = copy.deepcopy(classStrictMock)
        self.classMock.name = Mock(return_value="Bar")
        self.classMock.namespace = Mock(return_value="Aaa::Bbb::")

        self.filename = "Bar.hpp"
        self.testsuiteName = self.classMock.name() + "TestSuite"
        self.testsuiteNamespace = self.classMock.namespace();

    def testTestSuiteClass(self):
        s = "class BarTestSuite : public Test\n"
        s+= "{\n"
        s+= "};\n"
        self.assertEqual(s, testSuiteClass(self.testsuiteName))

    def testTestDummy(self):
        s  = "TEST_F("+self.testsuiteName+", TestDummy)\n"
        s += "{\n"
        s += "    EXPECT_TRUE(false);\n"
        s += "}\n"
        self.assertEqual(s, testSuiteTestDummy(self.testsuiteName))

    def testTestSuiteBody(self):
        s  = "using namespace ::testing;\n"
        s += "\n"
        s += testSuiteClass(self.testsuiteName)
        s += "\n"
        s += testSuiteTestDummy(self.testsuiteName)
        self.assertEqual(s, testSuiteBody(self.testsuiteName))

    def testTestSuiteFile(self):
        content = testSuiteBody(self.testsuiteName)
        local_includes = [self.filename]
        global_includes = [GTEST_HEADER]
        s = createSourceFile(content, self.testsuiteNamespace, local_includes, global_includes)
        file = TestSuiteFile(self.classMock, self.filename)
        self.assertEqual(s, str(file))
        self.assertEqual("AaaBbbBarTestSuite.cpp", file.name)


class BoolQuestionTestCase(unittest.TestCase):
    def setUp(self):
        self.question = "WHY?"
        self.inputMock = MagicMock()

    def testInputY(self):
        with patch("builtins.input", MagicMock(side_effect="Y")):
            self.assertTrue(boolQuestion(self.question))

    def testInputN(self):
        with patch("builtins.input", MagicMock(side_effect="N")):
            self.assertFalse(boolQuestion(self.question))

    def testInputInvalidThanY(self):
        with patch("builtins.input", MagicMock(side_effect=["ewq","Y"])):
            self.assertTrue(boolQuestion(self.question))

    def testInputInvalidThanN(self):
        with patch("builtins.input", MagicMock(side_effect=[" ","N"])):
            self.assertFalse(boolQuestion(self.question))

    def testInputInvalidThanInvalidThanInvalidThanY(self):
        with patch("builtins.input", MagicMock(side_effect=[" "," ","few","Y"])):
            self.assertTrue(boolQuestion(self.question))


if __name__ == "__main__":
    unittest.main()

