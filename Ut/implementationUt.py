#!/usr/bin/env python3
import unittest
import copy
from gen.implementation import *
from gen.CppHeaderParserWrappers import *
from mock import Mock
from CppWrappersMocks import *

class FunctionsImplFromClassTestCase(unittest.TestCase):

    def setUp(self):
        self.classMock = copy.deepcopy(classStrictMock)
        self.classMock.name = Mock(return_value = "Bar")
        self.classMock.methods = Mock(return_value = [])

        self.functionMock = copy.deepcopy(functionDefaultMock)
        self.paramMock = copy.deepcopy(parameterDefaultMock)

    def testFunctionsImplFromClassNoFunctions(self) :
        self.assertEqual("", functionsImplFromClass(self.classMock))

    def testFunctionsImplFromClassNoParams(self) :
        self.classMock.methods.return_value = [[self.functionMock]]
        s = "void Bar::foo()\n"
        s+= "{\n"
        s+= "}\n"
        self.assertEqual(s, functionsImplFromClass(self.classMock))

    def testPublicFunctionWithoutParamName(self) :
        self.classMock.methods.return_value = [[self.functionMock]]
        self.functionMock.parameters.return_value = [self.paramMock]
        s = "void Bar::foo(int p_param1)\n"
        s+= "{\n"
        s+= "}\n"
        self.assertEqual(s, functionsImplFromClass(self.classMock))

    def testPublicFunctionWithParamName(self) :
        self.classMock.methods.return_value = [[self.functionMock]]
        self.functionMock.parameters.return_value = [self.paramMock]
        self.paramMock.name.return_value = "p_id"
        s = "void Bar::foo(int p_id)\n"
        s+= "{\n"
        s+= "}\n"
        self.assertEqual(s, functionsImplFromClass(self.classMock))

class FunctionImplNeededTestCase(unittest.TestCase):
    def setUp(self) :
        self.functionMock = copy.deepcopy(functionStrictMock)
        self.functionMock.isPureVirtual = Mock(return_value=False)
        self.functionMock.isDeclaration = Mock(return_value=False)

    def testIsFunctionImplNeeded_notDeclarationNotPureVirtual(self) :
        self.assertFalse(isFunctionImplNeeded(self.functionMock))

    def testIsFunctionImplNeeded_isDeclarationNotPureVirtual(self) :
        self.functionMock.isDeclaration.return_value = True
        self.assertTrue(isFunctionImplNeeded(self.functionMock))

    def testIsFunctionImplNeeded_notDeclarationIsPureVirtual(self) :
        self.functionMock.isPureVirtual.return_value = True
        self.assertFalse(isFunctionImplNeeded(self.functionMock))

    def testIsFunctionImplNeeded_isDeclarationIsPureVirtual(self) :
        self.functionMock.isPureVirtual.return_value = True
        self.functionMock.isDeclaration.return_value = True
        self.assertFalse(isFunctionImplNeeded(self.functionMock))


class ImplementationFileTestCase(unittest.TestCase):
    def setUp(self) :
        self.maxDiff = None
        self.functionMock1 = copy.deepcopy(functionDefaultMock)
        self.functionMock1.name.return_value = "foo"
        self.functionMock2 = copy.deepcopy(functionDefaultMock)
        self.functionMock2.name.return_value = "foo1"

        self.classMock = copy.deepcopy(classStrictMock)
        self.classMock.name = Mock(return_value = "Bar")
        self.classMock.namespace = Mock(return_value = "Aaa::Bbb::")
        self.classMock.methods = Mock(return_value = [[self.functionMock1, self.functionMock2]])

    def testImplementationFile(self):
        file = "Asd.hpp"

        functions = "void Bar::foo()\n"
        functions+= "{\n"
        functions+= "}\n"
        functions+= "\n"
        functions+= "void Bar::foo1()\n"
        functions+= "{\n"
        functions+= "}\n"

        s = fileHeader("Aaa")
        s+= "\n"
        s+= "#include \"%s\"\n" % file
        s+= "\n"
        s+= wrapNamespaces("Aaa::Bbb::", functions)
        s+= "\n"

        self.assertEqual(s, str(ImplementationFile(self.classMock, file)))

if __name__ == "__main__":
    unittest.main()

