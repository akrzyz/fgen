#!/usr/bin/env python3
import unittest
from mock import MagicMock
from mock import Mock
import copy
from  gen.header import *
from  gen.CppHeaderParserWrappers import *
from CppWrappersMocks import *

class HeaderTestCase(unittest.TestCase):
    def setUp(self) :

        functionBaseMock = copy.deepcopy(functionDefaultMock)
        functionBaseMock.isVirtual.return_value = True
        functionBaseMock.isPureVirtual.return_value = True

        self.publicFunctionMock = copy.deepcopy(functionBaseMock)
        self.publicFunctionMock.name.return_value = "publicFoo"

        self.protectedFunctionMock = copy.deepcopy(functionBaseMock)
        self.protectedFunctionMock.name.return_value = "protectedFoo"

        self.privateFunctionMock = copy.deepcopy(functionBaseMock)
        self.privateFunctionMock.name.return_value = "privateFoo"

        self.notAbstractFunctionMock = copy.deepcopy(functionBaseMock)
        self.notAbstractFunctionMock.name.return_value = "notAbstractFoo"
        self.notAbstractFunctionMock.isPureVirtual.return_value = False

        self.classMock = copy.deepcopy(classStrictMock)
        self.classMock.name = Mock(return_value = "IBar")
        self.classMock.namespace = Mock(return_value = "Aaa::Bbb::")
        self.classMock.publicMethods    = Mock(return_value = [self.publicFunctionMock, self.notAbstractFunctionMock])
        self.classMock.protectedMethods = Mock(return_value = [self.protectedFunctionMock])
        self.classMock.privateMethods   = Mock(return_value = [self.privateFunctionMock])

    def testHeaderClass(self) :
        s = "class Qwe : public IBar\n"
        s+= "{\n"
        s+= "public:\n"
        s+= "    void publicFoo() override;\n"
        s+= "protected:\n"
        s+= "    void protectedFoo() override;\n"
        s+= "private:\n"
        s+= "    void privateFoo() override;\n"
        s+= "};\n"

        self.assertEqual(s, str(HeaderClass(self.classMock, "Qwe")))

    def testHeaderFile(self) :
        ifFile = "AaaBbbIBar.hpp"
        fileName = "AaaBbbBar.hpp"

        body  = "#include \"AaaBbbIBar.hpp\"\n"
        body += "\n"
        body += wrapNamespaces(self.classMock.namespace(), str(HeaderClass(self.classMock, "Bar")))

        s = fileHeader("Aaa")
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= body
        s+= "\n"

        headerFile = HeaderFile(self.classMock, ifFile)
        self.assertEqual(fileName, headerFile.name)
        self.assertEqual(s, str(headerFile))

class HasAbstractMethodsOrVirtualDestructor(unittest.TestCase):
    def setUp(self) :
        self.functionMock = Mock(spec=True)
        self.functionMock.isVirtual     = Mock(return_value = False)
        self.functionMock.isDestructor  = Mock(return_value = False)
        self.functionMock.isPureVirtual = Mock(return_value = False)
        self.classMock = Mock(spec=True)
        self.classMock.methods = Mock(return_value = [[self.functionMock]])

    def testAllFalse(self) :
        self.assertFalse(hasAbstractMethodsOrVirtualDestructor(self.classMock))

    def testIsVirtualBuntNotPure(self) :
        self.functionMock.isVirtual.return_value = True
        self.assertFalse(hasAbstractMethodsOrVirtualDestructor(self.classMock))

    def testIsDestructor(self) :
        self.functionMock.isDestructor.return_value = True
        self.assertFalse(hasAbstractMethodsOrVirtualDestructor(self.classMock))

    def testIsPureVirtual(self) :
        self.functionMock.isPureVirtual.return_value = True
        self.assertTrue(hasAbstractMethodsOrVirtualDestructor(self.classMock))

    def testIsVirtualDestructor(self) :
        self.functionMock.isDestructor.return_value = True
        self.functionMock.isVirtual.return_value = True
        self.assertTrue(hasAbstractMethodsOrVirtualDestructor(self.classMock))


if __name__ == "__main__":
    unittest.main()

