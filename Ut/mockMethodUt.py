#!/usr/bin/env python3
import unittest
import copy
from  gen.mockMethod import *
from  gen.CppHeaderParserWrappers import *
from  CppWrappersMocks import *

class MockedMethodTestCase(unittest.TestCase):

    def setUp(self):
        self.functionMock = copy.deepcopy(functionDefaultMock)
        self.intParamMock = copy.deepcopy(parameterStrictMock)
        self.intParamMock.type = Mock(return_value = "int")
        self.constStrRefParamMock = copy.deepcopy(parameterStrictMock)
        self.constStrRefParamMock.type = Mock(return_value = "const std::str &")

    def testVoidFoo0Params(self):
        s = "    MOCK_METHOD0(foo, void());\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testVoidFoo1SimplyParam(self):
        self.functionMock.parameters.return_value = [self.intParamMock]
        s = "    MOCK_METHOD1(foo, void(int));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testVoidFoo1ConstRefParam(self):
        self.functionMock.parameters.return_value = [self.constStrRefParamMock]
        s = "    MOCK_METHOD1(foo, void(const std::str &));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testVoidFoo2SimplyParamConstRef(self):
        self.functionMock.parameters.return_value = [self.intParamMock, self.constStrRefParamMock]
        s = "    MOCK_METHOD2(foo, void(int, const std::str &));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testVoidConstFoo1SimplyParam(self):
        self.functionMock.parameters.return_value = [self.intParamMock]
        self.functionMock.isConst.return_value = True
        s = "    MOCK_CONST_METHOD1(foo, void(int));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testCharFoo1SimplyParam(self):
        self.functionMock.parameters.return_value = [self.intParamMock]
        self.functionMock.returnType.return_value = "char"
        s = "    MOCK_METHOD1(foo, char(int));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testConstDoubleRefFoo1SimplyParam(self):
        self.functionMock.parameters.return_value = [self.intParamMock]
        self.functionMock.returnType.return_value = "const double &"
        s = "    MOCK_METHOD1(foo, const double &(int));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))

    def testIntConstFoo1SimplyParam(self):
        self.functionMock.parameters.return_value = [self.intParamMock]
        self.functionMock.returnType.return_value = "float"
        self.functionMock.isConst.return_value = True
        s = "    MOCK_CONST_METHOD1(foo, float(int));\n"
        self.assertEqual(s, MockedMethod(self.functionMock))


class MockedOperatorTestCase(unittest.TestCase):

    def setUp(self):
        self.operatorMock = copy.deepcopy(operatorDefaultMock)
        self.intParamMock = copy.deepcopy(parameterStrictMock)
        self.intParamMock.type = Mock(return_value = "int")
        self.intParamMock.name = Mock(return_value = "p_id")
        self.dummyRefParamMock = copy.deepcopy(parameterStrictMock)
        self.dummyRefParamMock.type = Mock(return_value = "Dummy &")
        self.dummyRefParamMock.name = Mock(return_value = "p_dummy")
        self.fooParamMock = copy.deepcopy(parameterStrictMock)
        self.fooParamMock.type = Mock(return_value = "Foo")
        self.fooParamMock.name = Mock(return_value = "p_foo")
        self.constFooRefParamMock = copy.deepcopy(parameterStrictMock)
        self.constFooRefParamMock.type = Mock(return_value = "const Foo &")
        self.constFooRefParamMock.name = Mock(return_value = "p_foo")
        self.sizeTParamMock = copy.deepcopy(parameterStrictMock)
        self.sizeTParamMock.type = Mock(return_value = "size_t")
        self.sizeTParamMock.name = Mock(return_value = "p_size")
        self.voidPtrParamMock = copy.deepcopy(parameterStrictMock)
        self.voidPtrParamMock.type = Mock(return_value = "void *")
        self.voidPtrParamMock.name = Mock(return_value = "p_ptr")

    def testAssignOperator(self):
        self.operatorMock.name.return_value += "="
        self.operatorMock.returnType.return_value = "Foo &"
        self.operatorMock.parameters.return_value = [self.constFooRefParamMock]
        s = "    MOCK_METHOD1(assignmentOperator, Foo &(const Foo &));\n"
        s+= "    virtual Foo & operator=(const Foo & p_foo)\n"
        s+= "    {\n"
        s+= "        return assignmentOperator(p_foo);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testEqualOperator(self):
        self.operatorMock.name.return_value += "=="
        self.operatorMock.returnType.return_value = "bool"
        self.operatorMock.parameters.return_value = [self.constFooRefParamMock]
        s = "    MOCK_METHOD1(equalToOperator, bool(const Foo &));\n"
        s+= "    virtual bool operator==(const Foo & p_foo)\n"
        s+= "    {\n"
        s+= "        return equalToOperator(p_foo);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testNotEqualOperator(self):
        self.operatorMock.name.return_value += "!="
        self.operatorMock.returnType.return_value = "bool"
        self.operatorMock.parameters.return_value = [self.constFooRefParamMock]
        s = "    MOCK_METHOD1(notEqualToOperator, bool(const Foo &));\n"
        s+= "    virtual bool operator!=(const Foo & p_foo)\n"
        s+= "    {\n"
        s+= "        return notEqualToOperator(p_foo);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testLessOperator(self):
        self.operatorMock.name.return_value += "<"
        self.operatorMock.returnType.return_value = "bool"
        self.operatorMock.parameters.return_value = [self.constFooRefParamMock]
        s = "    MOCK_METHOD1(lessThanOperator, bool(const Foo &));\n"
        s+= "    virtual bool operator<(const Foo & p_foo)\n"
        s+= "    {\n"
        s+= "        return lessThanOperator(p_foo);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testfunctionCall1ParamWithNameOperator(self):
        self.operatorMock.name.return_value += "()"
        self.operatorMock.returnType.return_value = "float"
        self.operatorMock.parameters.return_value = [self.intParamMock]
        s = "    MOCK_METHOD1(functionCallOperator, float(int));\n"
        s+= "    virtual float operator()(int p_id)\n"
        s+= "    {\n"
        s+= "        return functionCallOperator(p_id);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testfunctionCall2ParamWithNameOperator(self):
        self.operatorMock.name.return_value += "()"
        self.operatorMock.returnType.return_value = "float"
        self.operatorMock.parameters.return_value = [self.intParamMock, self.dummyRefParamMock]
        s = "    MOCK_METHOD2(functionCallOperator, float(int, Dummy &));\n"
        s+= "    virtual float operator()(int p_id, Dummy & p_dummy)\n"
        s+= "    {\n"
        s+= "        return functionCallOperator(p_id, p_dummy);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testfunctionCall2ParamWithoutNameOperator(self):
        self.operatorMock.name.return_value += "()"
        self.operatorMock.returnType.return_value = "float"
        self.intParamMock.name.return_value = ""
        self.dummyRefParamMock.name.return_value = ""
        self.operatorMock.parameters.return_value = [self.intParamMock, self.dummyRefParamMock]
        s = "    MOCK_METHOD2(functionCallOperator, float(int, Dummy &));\n"
        s+= "    virtual float operator()(int p_param1, Dummy & p_param2)\n"
        s+= "    {\n"
        s+= "        return functionCallOperator(p_param1, p_param2);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testfunctionCall2Param1WithName1WithoutNameOperator(self):
        self.operatorMock.name.return_value += "()"
        self.operatorMock.returnType.return_value = "float"
        self.dummyRefParamMock.name.return_value = ""
        self.operatorMock.parameters.return_value = [self.intParamMock, self.dummyRefParamMock]
        s = "    MOCK_METHOD2(functionCallOperator, float(int, Dummy &));\n"
        s+= "    virtual float operator()(int p_id, Dummy & p_param1)\n"
        s+= "    {\n"
        s+= "        return functionCallOperator(p_id, p_param1);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testfunctionCall0ParamsOperator(self):
        self.operatorMock.name.return_value += "()"
        self.operatorMock.returnType.return_value = "float"
        s = "    MOCK_METHOD0(functionCallOperator, float());\n"
        s+= "    virtual float operator()()\n"
        s+= "    {\n"
        s+= "        return functionCallOperator();\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testNewOperator(self):
        self.operatorMock.name.return_value += " new"
        self.operatorMock.returnType.return_value = "void *"
        self.operatorMock.parameters.return_value = [self.sizeTParamMock]
        s = "    MOCK_METHOD1(newOperator, void *(size_t));\n"
        s+= "    virtual void * operator new(size_t p_size)\n"
        s+= "    {\n"
        s+= "        return newOperator(p_size);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testNewArrayOperator(self):
        self.operatorMock.name.return_value += " new[]"
        self.operatorMock.returnType.return_value = "void *"
        self.operatorMock.parameters.return_value = [self.sizeTParamMock]
        s = "    MOCK_METHOD1(newArrayOperator, void *(size_t));\n"
        s+= "    virtual void * operator new[](size_t p_size)\n"
        s+= "    {\n"
        s+= "        return newArrayOperator(p_size);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testDeleteOperator(self):
        self.operatorMock.name.return_value += " delete"
        self.operatorMock.returnType.return_value = "void"
        self.operatorMock.parameters.return_value = [self.voidPtrParamMock]
        s = "    MOCK_METHOD1(deleteOperator, void(void *));\n"
        s+= "    virtual void operator delete(void * p_ptr)\n"
        s+= "    {\n"
        s+= "        return deleteOperator(p_ptr);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testDeleteArrayOperator(self):
        self.operatorMock.name.return_value += " delete[]"
        self.operatorMock.returnType.return_value = "void"
        self.operatorMock.parameters.return_value = [self.voidPtrParamMock]
        s = "    MOCK_METHOD1(deleteArrayOperator, void(void *));\n"
        s+= "    virtual void operator delete[](void * p_ptr)\n"
        s+= "    {\n"
        s+= "        return deleteArrayOperator(p_ptr);\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testConversionToInt(self):
        self.operatorMock.name.return_value += " int"
        self.operatorMock.returnType.return_value = ""
        s = "    MOCK_METHOD0(conversionToIntOperator, int());\n"
        s+= "    virtual operator int()\n"
        s+= "    {\n"
        s+= "        return conversionToIntOperator();\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

    def testConversionToConstIntRef(self):
        self.operatorMock.name.return_value += " const int &"
        self.operatorMock.returnType.return_value = ""
        s = "    MOCK_METHOD0(conversionToConstIntRefOperator, const int &());\n"
        s+= "    virtual operator const int &()\n"
        s+= "    {\n"
        s+= "        return conversionToConstIntRefOperator();\n"
        s+= "    }\n"
        self.assertEqual(s, MockedMethod(self.operatorMock))

if __name__ == "__main__":
    unittest.main()
