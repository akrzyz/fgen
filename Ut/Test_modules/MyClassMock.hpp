/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef MYCLASSMOCK_HPP
#define MYCLASSMOCK_HPP

#include <gmock/gmock.h>
#include <IOperators.hpp>

namespace Ut
{

struct MyClassMock : public IMyClass
{
    MOCK_METHOD1(additionAssignmentOperator, IBcchModIndReqWrapper &(const EPagingIndication));
    virtual IBcchModIndReqWrapper & operator+=(const EPagingIndication p_paging)
    {
        return additionAssignmentOperator(p_paging);
    }
    MOCK_CONST_METHOD1(lessThanOperator, bool(const MessageIdentifier &));
    virtual bool operator<(const MessageIdentifier & p_rhs) const
    {
        return lessThanOperator(p_rhs);
    }
    MOCK_CONST_METHOD1(functionCallOperator, bool(const u16));
    virtual bool operator()(const u16 p_warningMsgId) const
    {
        return functionCallOperator(p_warningMsgId);
    }
    MOCK_CONST_METHOD3(functionCallOperator, bool(const u16, int, const IAa &));
    virtual bool operator()(const u16 p_param1, int p_param2, const IAa & p_param3) const
    {
        return functionCallOperator(p_param1, p_param2, p_param3);
    }
    MOCK_CONST_METHOD4(functionCallOperator, bool(const u16, int, const IAa *, const IQq &));
    virtual bool operator()(const u16 p_param1, int p_id, const IAa * p_param2, const IQq & p_iq) const
    {
        return functionCallOperator(p_param1, p_id, p_param2, p_iq);
    }
    MOCK_METHOD1(newOperator, void *(size));
    virtual void * operator new(size T)
    {
        return newOperator(T);
    }
    MOCK_METHOD1(newArrayOperator, void *(size));
    virtual void * operator new[](size T)
    {
        return newArrayOperator(T);
    }
    MOCK_METHOD0(conversionToToTypeOperator, ToType());
    virtual operator ToType()
    {
        return conversionToToTypeOperator();
    }
    MOCK_METHOD0(conversionToConstToTypeOperator, const ToType());
    virtual operator const ToType()
    {
        return conversionToConstToTypeOperator();
    }
    MOCK_METHOD0(conversionToConstToTypeRefOperator, const ToType &());
    virtual operator const ToType &()
    {
        return conversionToConstToTypeRefOperator();
    }
    MOCK_METHOD0(conversionToToTypePtrOperator, ToType *());
    virtual operator ToType *()
    {
        return conversionToToTypePtrOperator();
    }
    MOCK_METHOD0(conversionToIntOperator, int());
    virtual operator int()
    {
        return conversionToIntOperator();
    }
};

} //namespace Ut

#endif //MYCLASSMOCK_HPP

