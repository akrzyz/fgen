/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef ENBCLOADBALANCINGCLIENTMOCK_HPP
#define ENBCLOADBALANCINGCLIENTMOCK_HPP

#include <gmock/gmock.h>
#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{
namespace Ut
{

struct ClientMock : public Client
{
    MOCK_METHOD1(setup, void(int));
    MOCK_METHOD1(setup, void(int *));
    MOCK_METHOD1(setup2, void(int));
    MOCK_CONST_METHOD2(start, bool(const std::string &, int));
    MOCK_CONST_METHOD1(start, bool(const std::string &));
    MOCK_CONST_METHOD2(start, bool(const char *, int));
    MOCK_METHOD0(stop, bool());
};

} //namespace Ut
} //namespace LoadBalancing
} //namespace Enbc

#endif //ENBCLOADBALANCINGCLIENTMOCK_HPP

