/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef ENBCLOADBALANCINGTTTMOCK_HPP
#define ENBCLOADBALANCINGTTTMOCK_HPP

#include <gmock/gmock.h>
#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{
namespace Ut
{

struct TttMock : public Ttt
{
};

} //namespace Ut
} //namespace LoadBalancing
} //namespace Enbc

#endif //ENBCLOADBALANCINGTTTMOCK_HPP

