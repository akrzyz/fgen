/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef ENBCLOADBALANCINGIAAAINERIAAAMOCK_HPP
#define ENBCLOADBALANCINGIAAAINERIAAAMOCK_HPP

#include <gmock/gmock.h>
#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{
namespace IAaa
{
namespace Ut
{

struct InerIAAAMock : public InerIAAA
{
    MOCK_METHOD0(asd, void());
};

} //namespace Ut
} //namespace IAaa
} //namespace LoadBalancing
} //namespace Enbc

#endif //ENBCLOADBALANCINGIAAAINERIAAAMOCK_HPP

