/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef ENBCLOADBALANCINGINTRAFREQCELLMOCK_HPP
#define ENBCLOADBALANCINGINTRAFREQCELLMOCK_HPP

#include <gmock/gmock.h>
#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{
namespace Ut
{

struct IntraFreqCellMock : public IntraFreqCell
{
    MOCK_METHOD0(foo, void());
};

} //namespace Ut
} //namespace LoadBalancing
} //namespace Enbc

#endif //ENBCLOADBALANCINGINTRAFREQCELLMOCK_HPP

