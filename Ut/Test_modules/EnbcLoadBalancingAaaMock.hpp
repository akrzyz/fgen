/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#ifndef ENBCLOADBALANCINGAAAMOCK_HPP
#define ENBCLOADBALANCINGAAAMOCK_HPP

#include <gmock/gmock.h>
#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{
namespace Ut
{

struct AaaMock : public IAaa
{
    MOCK_METHOD0(abstractFoo, void());
};

} //namespace Ut
} //namespace LoadBalancing
} //namespace Enbc

#endif //ENBCLOADBALANCINGAAAMOCK_HPP

