/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{

void QQQ::foo()
{
}

} //namespace LoadBalancing
} //namespace Enbc

