/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{

void Client::setup(int p_param1)
{
}

void Client::setup(int * p_param1)
{
}

void Client::setup2(int p_id)
{
}

bool Client::start(const std::string & p_param1, int p_param2) const
{
}

bool Client::start(const std::string & p_name) const
{
}

bool Client::start(const char * p_param1, int p_id) const
{
}

bool Client::stop()
{
}

void Client::dumy()
{
}

void Client::dumyPrivat()
{
}

} //namespace LoadBalancing
} //namespace Enbc

