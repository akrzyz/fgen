/*********************************************************************************
* @file                  $HeadURL:$
* @version               $LastChangedRevision:$
* @date                  $LastChangedDate:$
* @author                $Author:$
*
* @module                Enbc
* @owner                 LTE Control Plane
*
* Copyright 2014 Nokia Siemens Networks. All rights reserved.
**********************************************************************************/

#include <dupa.hpp>

namespace Enbc
{
namespace LoadBalancing
{

NoVirtual::NoVirtual(int p_param1)
{
}

void NoVirtual::abstractFoo()
{
}

} //namespace LoadBalancing
} //namespace Enbc

