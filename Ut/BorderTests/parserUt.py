#!/usr/bin/env python3
import os
import unittest
from gen import parser_utils
from gen import utils
from gen.consts import *

class DirectorySetter :
    def __init__(self):
        self.base = self.getBaseDir()

    def getBaseDir(self) :
        if os.path.exists("dumy.hpp") :
            baseDir = "."
        elif os.path.exists(os.path.join("BorderTests","dumy.hpp")) :
            baseDir = os.path.join(".","BorderTests")
        elif os.path.exists(os.path.join("Ut","BorderTests","dumy.hpp")) :
            baseDir = os.path.join(".","Ut","BorderTests")
        else :
            raise EnvironmentError("Wrong direcotry to start AcceptanceTests")
        return baseDir

directory = DirectorySetter().base

class ParsesTestCase(unittest.TestCase):
    def setUp(self):
        self.cppHeader = parser_utils.parseHppFileNotWrapped(os.path.join(directory,"dumy.hpp"))
        utils.writeToFile(os.path.join(directory,"parsedDummyHpp.txt"), repr(self.cppHeader))
        utils.writeToFile(os.path.join(directory,"parsedDummyHpp.txt"), repr(dir(self.cppHeader)), "a")
        utils.writeToFile(os.path.join(directory,"parsedDummyHpp.txt"), "\n"+str(self.cppHeader.functions), "a")

    def checkMethod(self, p_method,
                          p_name,
                          p_rtnType,
                          p_params = [],
                          p_namespace="AAA::BBB::",
                          p_virtual = True,
                          p_const = False,
                          p_static = False,
                          p_abstract = False):

        self.assertEqual(p_name     , p_method[NAME])
        self.assertEqual(p_rtnType  , p_method[RETURN_TYPE])
        self.assertEqual(p_namespace, p_method[NAMESPACE])
        self.assertEqual(p_virtual  , p_method[VIRTUAL])
        self.assertEqual(p_const    , p_method[CONST])
        self.assertEqual(p_static   , p_method[STATIC])
        self.assertEqual(p_abstract , p_method[PURE_VIRTUAL])

        self.assertEqual(len(p_params), len(p_method[PARAMETERS]))
        for i in range(len(p_params)) :
            self.assertEqual(p_params[i][NAME], p_method[PARAMETERS][i][NAME])
            self.assertEqual(p_params[i][TYPE], p_method[PARAMETERS][i][TYPE])

    def testParseIFunctions(self):
        self.assertTrue("IFunctions" in self.cppHeader.classes)
        IFunctions = self.cppHeader.classes["IFunctions"]

        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][0], "foo1", "const std::string &", [{NAME:"", TYPE:"int"}], p_abstract=True)
        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][1], "foo2", "const std::string &", [{NAME:"", TYPE:"const int &"}], p_const=True)
        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][2], "foo3", "const std::string &", [{NAME:"p_id", TYPE:"long"}], p_abstract=True)
        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][3], "foo4", "const std::string &", [{NAME:"p_id", TYPE:"const long &"}])

        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][4], "multipleParamsMultiLineFoo", "void", [{NAME:"p_id", TYPE:"int"},
                                                                                                         {NAME:"", TYPE:"float"},
                                                                                                         {NAME:"p_name", TYPE:"std::string"}],
                                                                                                         p_virtual=False)

        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][5], "notVirtualMethod", "static void", [], p_virtual=False, p_static = True)
        self.checkMethod(IFunctions[METHODS][ACCESS_PUBLIC][6], "overridedMethod", "void", [], p_virtual=True)

        #free functions in header!
        freeFunctions = self.cppHeader.functions
        self.checkMethod(freeFunctions[0], "freeFunction", "int", [], p_virtual=False)

    def testParseIOperators(self):
        self.assertTrue("IOperators" in self.cppHeader.classes)
        IOperators = self.cppHeader.classes["IOperators"]

        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][0], "operator=="     , "bool"  , [{NAME:"", TYPE:"const IBar &"}])
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][1], "operator=="     , "bool"  , [{NAME:"p_id", TYPE:"const int &"}], p_const=True)
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][2], "operator()"     , "bool"  , [{NAME:"", TYPE:"const std::string &"}])
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][3], "operator()"     , "bool"  , [{NAME:"p_id", TYPE:"int"}])
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][4], "operator int"   , ""      , [])
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][5], "operator new"   , "void *", [{NAME:"", TYPE:"size_t"}])
        self.checkMethod(IOperators[METHODS][ACCESS_PUBLIC][6], "operator delete", "void"  , [{NAME:"", TYPE:"void *"}])

if __name__ == "__main__":
    unittest.main()

