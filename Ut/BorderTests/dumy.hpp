namespace AAA
{
namespace BBB
{

struct IFunctions
{
    virtual const std::string & foo1(int) = 0;
    virtual const std::string & foo2(const int &) const;
    virtual const std::string & foo3(long p_id) = 0;
    virtual const std::string & foo4(const long & p_id);
    
    void multipleParamsMultiLineFoo(int p_id,
                                    float,
                                    std::string p_name);

    static void notVirtualMethod();
    void overridedMethod() override;
};

struct IOperators
{
    virtual bool operator==(const IBar &); 
    virtual bool operator==(const int & p_id) const; 
    virtual bool operator()(const std::string &); 
    virtual bool operator()(int p_id); 
    virtual operator int();
    virtual void * operator new(size_t); 
    virtual void operator delete(void *); 
}

int freeFunction();

} //namespace AAA
} //namespace BBB
