#!/usr/bin/env python3
import copy
import unittest
from gen.factory import *
from gen.CppHeaderParserWrappers import *
from CppWrappersMocks import *

class FactoryFunctionsTestCase(unittest.TestCase):

    def setUp(self):
        classStrictMock.name = Mock(return_value = "Bar")
        classStrictMock.namespace = Mock(return_value = "Aaa::Bbb::")
        self.classMock = classStrictMock

    def testFactoryInterfaceClass(self):
        s = "class IBarFactory\n"
        s+= "{\n"
        s+= "public:\n"
        s+= "    virtual ~IBarFactory() {}\n"
        s+= "    virtual std::shared_ptr<Bar> create() = 0;\n"
        s+= "};\n"

        self.assertEqual(s, factoryInterface(self.classMock.name()))

    def testFactoryInterfaceFile(self):
        file = "AaaBbbBar.hpp"
        component = "Aaa"

        s = fileHeader(component)
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= "#include <memory>\n"
        s+= "#include \"AaaBbbBar.hpp\"\n"
        s+= "\n"
        s+= "namespace Aaa\n"
        s+= "{\n"
        s+= "namespace Bbb\n"
        s+= "{\n"
        s+= "\n"
        s+= factoryInterface(self.classMock.name())
        s+= "\n"
        s+= "} //namespace Bbb\n"
        s+= "} //namespace Aaa\n"
        s+= "\n"

        self.assertEqual(s, str(FactoryInterfaceFile(self.classMock, file)))

if __name__ == "__main__":
    unittest.main()

