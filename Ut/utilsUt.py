#!/usr/bin/env python3
import unittest
from gen.utils import *
from mock import patch, MagicMock

class UtilsTestCase(unittest.TestCase):

    def testAddIndent(self) :
        string  = "aaaa\n"
        string += "  Bbbb\n"
        string += "Cccc\n"
        indent = "    "
        ecpectedString  = indent + "aaaa\n"
        ecpectedString += indent + "  Bbbb\n"
        ecpectedString += indent + "Cccc\n"
        self.assertEqual(ecpectedString, addIndent(string,indent))

    def testRemoveILikieInterface(self):
        self.assertEqual("IQwerty",  addILikieInterface("Qwerty"))
        self.assertEqual("IIntra",   addILikieInterface("Intra"))
        self.assertEqual("Iintra",   addILikieInterface("intra"))
        self.assertEqual("IQwerty",  addILikieInterface("IQwerty"))
        self.assertEqual("IIntra",   addILikieInterface("IIntra"))
        self.assertEqual("IiIntra",  addILikieInterface("iIntra"))

    def testAddILikieInterface(self):
        self.assertEqual("Qwerty", removeILikieInterface("Qwerty"))
        self.assertEqual("Intra",  removeILikieInterface("Intra"))
        self.assertEqual("intra",  removeILikieInterface("intra"))
        self.assertEqual("Qwerty", removeILikieInterface("IQwerty"))
        self.assertEqual("Intra",  removeILikieInterface("IIntra"))
        self.assertEqual("iIntra", removeILikieInterface("iIntra"))

    def testCapitalizeFirstWordLetter(self) :
        self.assertEqual("I Like LTE And ENBs ToBoGood AndTaste", capitalizeFirstWordLetter("i like LTE and eNBs ToBoGood andTaste"))

class CheckersTestCase(unittest.TestCase):
    class Opt :
        args = []
    def setUp(self):
        self.opt = self.Opt()
        self.opt.args = ["Foo.hpp", "Bar.hpp"]

    def testChecker(self) :
        def isGreaterThanFive(i):
            return i > 5
        def isLessThanTen(i):
            return i < 10
        checker = Checker()
        checker.add_check(isGreaterThanFive)
        checker.add_check(isLessThanTen)
        self.assertTrue(checker.check(8))
        self.assertFalse(checker.check(3))
        self.assertFalse(checker.check(12))

    def testCheckAtLeastOneArg(self):
        self.opt.args = []
        self.assertFalse(checkAtLeastOneArg(self.opt))
        self.opt.args.append("Aaa::Bbb::Foo")
        self.assertTrue(checkAtLeastOneArg(self.opt))

    def testCheckFileFromArgsExist(self):
        def isNotDummyHpp(file) :
            if file == "dummy.hpp" :
                return False
            return True

        isfileMock = MagicMock()
        isfileMock.side_effect = isNotDummyHpp
        with patch("os.path.isfile", isfileMock):
            self.assertTrue(checkFileFromArgsExist(self.opt))
            self.opt.args.append("dummy.hpp")
            self.assertFalse(checkFileFromArgsExist(self.opt))

if __name__ == "__main__":
    unittest.main()

