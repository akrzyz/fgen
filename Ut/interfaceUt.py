#!/usr/bin/env python3
import unittest
from  gen.interface import *

class InterfaceTestCase(unittest.TestCase):

    def testInterfaceClass(self) :
        className = "IFoo"
        s = "class %s\n" % className
        s+= "{\n"
        s+= "public:\n"
        s+= "    virtual ~%s() {}\n" % className
        s+= "};\n"
        self.assertEqual(s, interfaceClass(className))

    def testInterfaceFile(self) :
        fullName  = "Aaa::Bbb::IBar"
        namespace = "Aaa::Bbb"
        component = "Aaa"
        className = "IBar"
        filename  = "AaaBbbIBar.hpp"
        s = fileHeader(component)
        s+= "\n"
        s+= headerGuard()
        s+= "\n"
        s+= wrapNamespaces(namespace, interfaceClass(className))
        s+= "\n"

        self.assertEqual(s, str(InterfaceFile(fullName)))

if __name__ == "__main__":
    unittest.main()
