#!/usr/bin/env python3
from optparse import OptionParser
from gen.implementation import *
from gen.utils import *
from gen.consts import *

def Options() :
    usage = "usage: %prog [OPTIONS]... FILE..."
    epilog = "example: %prog EnbcInterFreqFoo file EnbcInterFreqFoo.cpp will be generated in Source directory."
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option("-f", "--force", dest="force", action="store_true", default=False, help="Force file overwrite.")
    parser.add_option("-d", "--output_dir", dest="output_dir", default=DEFAULT_OUTPUT_DIR_IMPLEMENTATION, help="By default is: " + DEFAULT_OUTPUT_DIR_IMPLEMENTATION)
    (options, args) = parser.parse_args()
    options.args = args
    return options

def OptionsConsistencyChecker() :
    checker = Checker()
    checker.add_check(checkAtLeastOneArg)
    checker.add_check(checkFileFromArgsExist)
    return checker

def run(options, checker) :
    if checker.check(options) :
        inputHeaderFiles = options.args
        return generate(inputHeaderFiles, options.output_dir, options.force)

def generate(inputHeaderFiles, outputDirectory, force) :
    implementationFileGenerator = FileGenerator(ImplementationFilesFactory)
    return Generate(implementationFileGenerator, inputHeaderFiles, outputDirectory, force)

if __name__ == "__main__":
    options = Options()
    checker = OptionsConsistencyChecker()
    run(options, checker)

