[![Build Status](https://semaphoreci.com/api/v1/akrzyz/fgen/branches/master/badge.svg)](https://semaphoreci.com/akrzyz/fgen)

### What is this repository for? ###

* This is C++ sketch file generator

### Requirements ###

* python3

### How To ###

* run: fGen -h
* run unit tests: Ut/runUt.py