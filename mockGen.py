#!/usr/bin/env python3
from optparse import OptionParser
from gen.utils import *
from gen.mock import *
from gen.consts import *

def Options() :
    usage = "usage: %prog [OPTIONS]... FILE..."
    epilog = "example: %prog Include/IFoo.hpp will generate Test_modules/FooMock.hpp"
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option("-f", "--force", dest="force", action="store_true", default=False, help="Force file overwrite.")
    parser.add_option("-d", "--output_dir", dest="output_dir", default=DEFAULT_OUTPUT_DIR_MOCK, help="Mock file output directory, by default is: " + DEFAULT_OUTPUT_DIR_MOCK)
    (options, args) = parser.parse_args()
    options.args = args
    return options

def OptionsConsistencyChecker() :
    checker = Checker()
    checker.add_check(checkAtLeastOneArg)
    checker.add_check(checkFileFromArgsExist)
    return checker

def run(options, checker):
    if checker.check(options) :
        inputInterfaceFiles = options.args
        return generate(inputInterfaceFiles, options.output_dir, options.force)

def generate(inputInterfaceFiles, outputDirectory, force) :
    mockFileGenerator = FileGenerator(MockFilesFactory)
    return Generate(mockFileGenerator, inputInterfaceFiles, outputDirectory, force)

if __name__ == "__main__":
    options = Options()
    checker = OptionsConsistencyChecker()
    generate(options, checker)

