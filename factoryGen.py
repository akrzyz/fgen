#!/usr/bin/env python3
from optparse import OptionParser
from gen.factory import *
from gen.header import *
from gen.implementation import *
from gen.utils import *
from gen.consts import *

def Options() :
    usage = "usage: %prog [OPTIONS]... FILE..."
    epilog = "example: %prog EnbcInterFreqIFoo file EnbcInterFreqFoo.hpp will be generated in current directory."
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option("-f", "--force", dest="force", action="store_true", default=False, help="Force file overwrite.")
    parser.add_option("-d", "--output_dir", dest="output_dir", default=DEFAULT_OUTPUT_DIR_FACTORY_INTERFACE, help="By default is: " + DEFAULT_OUTPUT_DIR_FACTORY_INTERFACE)
    (options, args) = parser.parse_args()
    options.args = args
    return options

def OptionsConsistencyChecker() :
    checker = Checker()
    checker.add_check(checkAtLeastOneArg)
    checker.add_check(checkFileFromArgsExist)
    return checker

#TODO add proper output dirs
class FactoryFilesGenerator :
    def generate(self, inputFile, outputDir, force) :
        self.outputDir = outputDir
        self.force = force
        generatedInterfaceFilesNames = self._generateInterfaceFiles(inputFile)
        generatedHeaderFilesNames = self._generateHppFiles(generatedInterfaceFilesNames)
        generatedImplFilesNames = self._generateCppFiles(generatedHeaderFilesNames)
        return generatedInterfaceFilesNames + generatedHeaderFilesNames + generatedImplFilesNames

    def _generateInterfaceFiles(self, inputFile) :
        interfaceFileGenerator = FileGenerator(FactoryInterfaceFilesFactory)
        return Generate(interfaceFileGenerator, [inputFile], self.outputDir, self.force)

    def _generateHppFiles(self, factoryInterfaceFiles) :
        hppFileGenerator = FileGenerator(HeaderFilesFactory)
        return Generate(hppFileGenerator, factoryInterfaceFiles, self.outputDir, self.force)

    def _generateCppFiles(self, factoryHppFiles) :
        cppFileGenerator = FileGenerator(ImplementationFilesFactory)
        return Generate(cppFileGenerator, factoryHppFiles, self.outputDir, self.force)

def run(options, checker):
    if checker.check(options) :
        inputInterfaceFiles = options.args
        return generate(inputInterfaceFiles, options.output_dir, options.force)

def generate(inputInterfaceFiles, outputDirectory, force) :
        factoryFileGenerator = FactoryFilesGenerator()
        return Generate(factoryFileGenerator, inputInterfaceFiles, outputDirectory, force)

if __name__ == "__main__":
    options = Options()
    checker = OptionsConsistencyChecker()
    run(options, checker)

